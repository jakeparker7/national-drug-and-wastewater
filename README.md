# National Drug & Wastewater

## Product Description Website
The product description website allows members of the public access documentation and a brief summary of the web application designed by Nang-It. As well as providing information about the product, the website allows users to submit their email, as to express their interest with the product. The Product-Description API allows data queries to a database management system hosted on a UQ zone. It will also allow the insertion of data to the database in the form of a simple email. 

You can use your browser to access the product description website and is recommended to interact with the API using python. 

/API/product-website/submit-email/ POST
Params: email (String), date (String), time(String)
Returns: success or no success

/API/product-website/emails/ GET
Params: (UQ single signon token?)
Returns the list of emails submitted to the database
Example response:
{emails :
[
{email: example@gmail.com, date: 1/1/18, time: 15:32},
{email: example2@gmail.com, date: 2/1/18, time: 03:32}
]
}

To send POST and GET requests for the Emails table in our database. Send them to /emails. It is setup just like /users and /groups. So if you have your POST and GET requests working for /users and /groups then /emails should be no issues. (uh oh dont have users / groups set up)

Product
POST /API/data/drugs - send data to the backend
Params: csv - the csv file of data, site number , day, drug, value

POST /API/data/sites - create a new site or update a site.

/API/data/

Meeting Lucas, Aidan, Ray

/API/concentrations /drugs /report /site /flows

Data - Backend function to strip site number and day from sample name sent in as sheet

Database
Rename data table to concentrations
Make copy of data table called flows with no drug name
Add month to key of sites (keyed by site, year, month)

GET request for concentrations may have a type of drug passed as a paramater (eg GET /API/concentrations?MDMA)

https://github.com/pyexcel/pyexcel-xlsx

Ask researchers to convert file to CSV.

## Sample API calls

/API/Concentrations
Return all drug data contained in the form of a JSON with the key being the drug and the value being data. The data value contains an array of JSONs with three values - date, batch and concentration. Each JSON in this array is one day.
{data: 
{
drug: MDMA, data: [{date: Apr 2018, batch: 001_April2018_Day1, concentration: 130ml},{date: Apr 2018, batch: 002_April2018_Day2, concentration: 150ml}],
Drug: Meth….eg eg
}
}

/API/Concentrations/?DRUG
Return all drug for a specific drug type given by the drug paramater. Data contains an array of JSONs with each json holding three values - date, batch and concentration
{data: 
[{date: Apr 2018, batch: 001_April2018_Day1, concentration: 130ml},{date: Apr 2018, batch: 002_April2018_Day2, concentration: 150ml}]

}
