from django.db import models

"""
Model to store the emails of users who express their interest on the product 
website
"""
class Emails(models.Model):
    email = models.CharField(max_length = 100);
    user_ID = models.IntegerField();
    date = models.DateTimeField(auto_now=False, auto_now_add=True);
