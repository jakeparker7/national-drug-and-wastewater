import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { HashRouter } from 'react-router-dom'
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { unregister } from './registerServiceWorker'

ReactDOM.render(<HashRouter>
            		<App />
	              </HashRouter>,
                document.getElementById('root'));
unregister();
