import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router'
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import AccountCircle from '@material-ui/icons/AccountCircle'
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from '@material-ui/core/styles';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import axios from 'axios';
import blue from '@material-ui/core/colors/blue';

const theme = createMuiTheme({
  palette: {
    primary: blue, // Purple and green play nicely together.
    secondary: { main: '#ffffff' }
  },
});

class LoginForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username:'',
      password:'',
      loginStatus: false
    }
  }

  handlePassChange = this.handlePassChange.bind(this);
  handleUserChange = this.handleUserChange.bind(this);

  handleUserChange(event) {
    this.setState({
      username: event.target.value,
    });
  };

  handlePassChange(event) {
    this.setState({
      password: event.target.value,
    });
  }

  handleClick = (event) => {
    {/* Setup the base URL for the auth api*/}
    var apiBaseUrl = "https://deco3801-nang-it.uqcloud.net/API/";
    var self = this;
    console.log(this.state.username);
    console.log(this.state.password);
    var payload={
     "username":this.state.username,
     "password":this.state.password
    }

    {/*Post the request*/}
    axios({
      method: 'post',
      url: apiBaseUrl+'api-auth/login/',
      headers: {
            'Content-Type': 'application/json'
      },
      data: {
        username: this.state.username,
        password: this.state.password
      }
    }).then(function (response) {
    console.log(response);

    {/*OK Response*/}
    if(response.data.code == 200){
    console.log("Login successfull");
    } else if(response.data.code == 204){
      {/*Non valid / bad responses*/}
      console.log("Username password do not match");
      alert("username password do not match")
    } else {
      {/*Non valid / bad responses*/}
      console.log("Username does not exists");
      alert("Username does not exist");
    }
    }).catch(function (error) {
     self.setState({loginStatus:true});
     console.log("There was an error!");
     console.log(error);
    });

  };

  render() {
    return(
    <div>
    <MuiThemeProvider theme={theme}>
    <Paper className="paper login" elevation={5} style={{width:"30vh", height:"30vh"}}>
    <div style={{display: 'flex', justifyContent: 'center'}}>
        <Typography variant="headline" color="black">
        Please provide your details
        </Typography>
      </div>
        <br></br>
      <div style={{display: 'flex', justifyContent: 'center'}}>
          <TextField
              autoFocus
              margin="dense"
              id="user"
              label="Name"
              type="text"
              value={this.state.username}
              onChange={this.handleUserChange}
            />
        </div>
        <br></br>
        <div style={{display: 'flex', justifyContent: 'center'}}>
        <TextField
              autoFocus
              margin="normal"
              id="password"
              label="email"
              type="text"
              value={this.state.password}
              onChange={this.handlePassChange}
            />
            </div>
            <br></br>
            <div style={{display: 'flex', justifyContent: 'center'}}>
            <Button variant="contained" color="primary" onClick={(event) => this.handleClick(event)}>
            Login
            </Button>
            </div>
          </Paper>
          {this.state.loginStatus && (
          <Redirect to={'/researcher-portal'}/>
          )}
          </MuiThemeProvider>
          </div>
    )
  }
}

export default LoginForm
