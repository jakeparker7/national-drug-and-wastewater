import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

class EmailDataTable extends Component {

  render() {
    {/* data passed in is an array of rows with an id, author and message */}
    const rows = this.props.data
    return(
        <div>
        <div style={{margin:"2%", display: 'flex', justifyContent: 'center'}}>
        <h1> {this.props.title} </h1>
        </div>
        <Table>

      {/* Table header */}
      <TableHead>
          <TableRow>
            <TableCell>Email</TableCell>
            <TableCell>Date</TableCell>
            <TableCell>Time</TableCell>
          </TableRow>
        </TableHead>

      {/* Map the data into the table */}
      {rows.map(row => {
            return (
              <TableRow key={row.id}>
                <TableCell>{row.email}</TableCell>
                <TableCell>{row.date}</TableCell>
                <TableCell>{row.time}</TableCell>
              </TableRow>
            );
          })}
      </Table>
      </div>
    )
  }
}

export default EmailDataTable;
