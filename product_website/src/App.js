import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { BrowserRouter } from 'react-router-dom';

import logo from './logo.svg';
import './App.css';

import NangNavBar from './components/NangNavPWBar';
import home from './pages/home-product-website'
import stats from './pages/stats'

class App extends Component {
  render() {
    return (
        <div>
          <Route exact path="/" component={home} />
          <Route path="/stats" component={stats} />
          <Route exact path="/facebook-share" component={() => window.location = 'https://www.facebook.com/sharer/sharer.php?u=https%3A//deco3801-nang-it.uqcloud.net/product-website'} />
          <Route exact path="/twitter-share" component={() => window.location = 'https://twitter.com/home?status=Check%20out%20the%20new%20Australian%20National%20Wasterwater%20Drug%20Monitoring%20Program%20(NWDMP)%20website!%0Ahttps%3A//deco3801-nang-it.uqcloud.net/product-website'} />
          <Route exact path="/googleplus-share" component={() => window.location = 'https://plus.google.com/share?url=https%3A//deco3801-nang-it.uqcloud.net/product-website'} />
          <Route exact path="/auth" component={() => window.location = 'https://deco3801-nang-it.uqcloud.net/uqauth/index.php'} />
          </div>
    );
  }
}

export default App;
