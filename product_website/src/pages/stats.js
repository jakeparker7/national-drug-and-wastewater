import '../App.css';
import React, { Component } from 'react';
import NangNavBar from '../components/NangNavPWBar';
import pdf from '../nwdmp4.pdf'
import Typography from '@material-ui/core/Typography';
import RawDataTable from '../components/EmailDataTable'
import Paper from '@material-ui/core/Paper';
import axios from 'axios';

class stats extends Component {
    constructor(props) {
      super(props)
      this.handleLoadData = this.handleLoadData.bind(this);
      this.state = {
        data:[
          {
            "email": "example@gmail.com",
            "date": "1/1/2018",
            "time": "15:32"
          },
          {
            "email": "example2@gmail.com",
            "date": "1/1/2018",
            "time": "09:51"
          }],
        numEmails: 1,
        token: ""
      }
      this.handleLoadData();
    }

    handleLoadData = () => {
        var self = this;
        {/*Login with admin to get the token*/}
        axios({
          method: 'post',
          url: 'https://deco3801-nang-it.uqcloud.net/API/api-token-auth/',
          headers: {
                'Content-Type': 'application/json'
          },
          data: {
            username: "admin",
            password: "password123"
          }
        }).then(function (response) {
            console.log("Response")
            console.log(response.data);
        {/*OK Response*/}
            if(response.data.code == 200){
                console.log("Login successfull");
                self.setState({
                  token: response.data.token
                });
                console.log("Token set");
                return;
            } else if(response.data.code == 204){
              {/*Non valid / bad responses*/}
                console.log("Username password do not match");
                alert("username password do not match")
            }
        }).catch(function (error) {
            console.log("There was an error!");
            console.log(error);
        });

    };

    handleLoadEmails = () => {
    };
    render() {
        return (
          <div>
          <NangNavBar/>
          <div style={{margin:"2%", display: 'flex', justifyContent: 'center'}}>
                <h1>
                Total Emails submitted to page
                </h1>
            </div>
            <div style={{margin:"2%", display: 'flex', justifyContent: 'center'}}>
                  <h2>
                  {this.state.numEmails}
                  </h2>
          </div>
          <div style={{margin: '5% 5% 5% 5%'}}>
          <Paper>
            <RawDataTable title = "Email Data" data = {this.state.data}/>
          </Paper>
          </div>
          </div>

        )
    }
}

export default stats;
