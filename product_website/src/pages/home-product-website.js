import '../App.css';
import React, { Component } from 'react';
import NangNavBar from '../components/NangNavPWBar';
import MainFooter from '../components/MainFooter';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import { Link } from 'react-router-dom'
import YouTube from 'react-youtube';
import FacebookBox from 'mdi-material-ui/FacebookBox'
import TwitterBox from 'mdi-material-ui/TwitterBox'
import GooglePlusBox from 'mdi-material-ui/GooglePlusBox'
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import axios from 'axios';

const theme = createMuiTheme({
  palette: {
    primary: blue, // Purple and green play nicely together.
    secondary: { main: '#ffffff' }
  },
});

class HomeContent extends Component {

    state = {
        open: false,
        email: ""
    };

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    handleTextFieldChange = this.handleTextFieldChange.bind(this);

    handleTextFieldChange(event) {
        this.setState({
            email: event.target.value
        });
    };

    handleSubmit = (email) => {
        console.log(email);
        this.setState({ open: false });
        {/*Post the request*/}
        axios({
          method: 'post',
          url: 'https://deco3801-nang-it/API/emails',
          headers: {
                'Content-Type': 'application/json'
          },
          data: {
            email: this.state.email
          }
        }).then(function (response) {
        console.log("Response")
        console.log(response.data);

        {/*OK Response*/}
        if(response.data.code == 200){
            console.log("Login successfull");
        } else if(response.data.code == 204){
          {/*Non valid / bad responses*/}
            console.log("Username password do not match");
            alert("username password do not match")
        } else {
          {/*Non valid / bad responses*/}
            console.log("Username does not exists");
            alert("Username does not exist");
        }
        }).catch(function (error) {
            console.log("There was an error!");
            console.log(error);
        });
    };

    viewStats = () => {
        {/*Redirect to UQ single signon, get cookie*/}
        this.props.history.push('/auth')
    };

  render() {
    const opts = {
        height: '390',
        width: '640',
        playerVars: { // https://developers.google.com/youtube/player_parameters
            autoplay: 0
        }
    };
    return (
      <div>
      <NangNavBar/>
      <MuiThemeProvider theme={theme}>
      <div style={{margin: '5%'}}>
        <div style={{margin:"2%", display: 'flex', justifyContent: 'center'}}>
        <h1>
        National Wastewater Drug Monitoring Program - Development
        </h1>
        </div>
          <div style={{margin:"1%"}}>
            <Typography variant="headline" color="black">
            About The NWDMP
            </Typography>
            <p align="justify">
              The national wastewater drug monitoring program is a research project between the Univeristy of Queensland
              and the Australian Criminal Intelligence commision. The program collects wastewater samples and analyses them
              for drug content - helping to provide a overview of the Australian populations drug usage.
            </p>
            <div style={{margin:"2%", display: 'flex', justifyContent: 'center'}}>
                <img src={require('../resources/logos.png')} />
            </div>
          </div>

          <div style={{margin:"1%"}}>
            <Typography variant="headline" color="black">
            About The NWDMP Researcher Portal
            </Typography>
            <div style={{margin:"2%", display: 'flex', justifyContent: 'center'}}>
                <YouTube
                    videoId="pLTjaHYhj_Q"
                    opts={opts}
                    onReady={this._onReady}
                 />
             </div>
            <p align="justify">
              Our team (Nang-IT) is building a detailed researcher portal to help the researchers at the NWDMP more efficiently and
              effectively conduct their research and share it with the public.

              The researchers portal allows the senior researchers at the NWDMP to enter data (manually and via csv files) to
              a cloud database. They are able to store this data, edit it and delete it. From this data, researchers will be able
              to auto generate tables and graphs to be used in their quaterly reports.

              The portal is password protected by default, but through the teams temporary password system the senior researchers
              are able to provide temporary access to other researchers or the Australian Criminal Commision to provide them with full
              read-only access to the data.

              Want to help us out? Share our socials!
            </p>
          </div>

          <div style={{margin:"1%"}}>
            <Typography variant="headline" color="black">
            About The NWDMP Public Page
            </Typography>
            <p align="justify">
              Ontop of the researcher portal, our team is also building a public website for the NWDMP to get their research out to the general public.
              This website contains basic information about the NWDMP, links to all their published reports, and visualisations of the data which the
              researchers have permitted the public to see.
            </p>
          </div>

          <div style={{margin:"1%"}}>
            <Typography variant="headline" color="black">
            Register your interest
            </Typography>
            <p align="justify">
              The NWDMP Researcher portal and public page is set to be released in late 2018. If you would to be contacted upon its release,
              please register below. If you like the work we are doing, be sure to share us on your socials too!
            </p>
            <div style={{display: "flex", flexDirection:"row"}}>
            <div style={{padding:"3px"}}>
            <Button variant="contained" color="primary" onClick={this.handleClickOpen}>
            Get Notified About our release!
            </Button>
            </div>
            <div style={{padding:"3px"}}>
            <Button variant="contained" color="primary" onClick={this.viewStats}>
            View our registered emails
            </Button>
            </div>
            <Dialog
            open={this.state.open}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
            >
            <DialogTitle id="form-dialog-title">Register your interest</DialogTitle>
            <DialogContent>
            <DialogContentText>
              To get news on when the NWDMP website and portal is released, please
              provide your email below.
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Email Address"
              type="email"
              fullWidth
              onChange={this.handleTextFieldChange}
            />
          </DialogContent>
            <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={() => this.handleSubmit(this.state.email)} color="primary">
              Subscribe
            </Button>
            </DialogActions>
            </Dialog>
            </div>
            <Typography variant="headline" color="black">
            Share on our socials!
            </Typography>
            <div style={{margin:"1%"}}>
                <Link
                to="/facebook-share">
                <FacebookBox/>
                </Link>

                <Link
                to="/twitter-share">
                <TwitterBox/>
                </Link>

                <Link
                to="/googleplus-share">
                <GooglePlusBox/>
                </Link>
            </div>
        </div>
      </div>
      </MuiThemeProvider>
      </div>
    )
  }
}

export default HomeContent;
