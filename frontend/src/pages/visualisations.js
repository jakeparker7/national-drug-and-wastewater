import '../App.css';
import React, { Component } from 'react';
import DrugBarGraph from "../components/DrugBarGraph"
import NangNavBar from '../components/NangNavBar';
import axios from 'axios';
import DataAdapter from "../components/DataAdapter"
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

const sampleResponse = {'data' :{"response": 200,
      "data": [{"site_no_id": 3, "day": 1, "drug_id": "MDMA", "value": 1718, "date": "Jan 2017"},
       {"site_no_id": 3, "day": 2, "drug_id": "MDMA", "value": 994, "date": "Jan 2017"},
        {"site_no_id": 3, "day": 3, "drug_id": "MDMA", "value": 972, "date": "Jan 2017"},
         {"site_no_id": 3, "day": 4, "drug_id": "MDMA", "value": 800, "date": "Jan 2017"},
          {"site_no_id": 3, "day": 5, "drug_id": "MDMA", "value": 991, "date": "Jan 2017"},
           {"site_no_id": 3, "day": 6, "drug_id": "MDMA", "value": 1434, "date": "Jan 2017"},
            {"site_no_id": 2, "day": 7, "drug_id": "MDMA", "value": 2069, "date": "Jan 2017"},
            {"site_no_id": 2, "day": 1, "drug_id": "MDMA", "value": 859, "date": "Jan 2017"},
             {"site_no_id": 2, "day": 2, "drug_id": "MDMA", "value": 920, "date": "Jan 2017"},
              {"site_no_id": 2, "day": 3, "drug_id": "MDMA", "value": 1233, "date": "Jan 2017"},
               {"site_no_id": 2, "day": 4, "drug_id": "MDMA", "value": 674, "date": "Jan 2017"},
                {"site_no_id": 2, "day": 5, "drug_id": "MDMA", "value": 723, "date": "Jan 2017"},
                 {"site_no_id": 2, "day": 6, "drug_id": "MDMA", "value": 623, "date": "Jan 2017"},
                  {"site_no_id": 2, "day": 7, "drug_id": "MDMA", "value": 646, "date": "Jan 2017"},
                  {"site_no_id": 3, "day": 1, "drug_id": "MDMA", "value": 1718, "date": "Feb 2017"},
                   {"site_no_id": 3, "day": 2, "drug_id": "MDMA", "value": 838, "date": "Feb 2017"},
                    {"site_no_id": 3, "day": 3, "drug_id": "MDMA", "value": 562, "date": "Feb 2017"},
                     {"site_no_id": 3, "day": 4, "drug_id": "MDMA", "value": 885, "date": "Feb 2017"},
                      {"site_no_id": 3, "day": 5, "drug_id": "MDMA", "value": 745, "date": "Feb 2017"},
                       {"site_no_id": 3, "day": 6, "drug_id": "MDMA", "value": 781, "date": "Feb 2017"},
                        {"site_no_id": 3, "day": 7, "drug_id": "MDMA", "value": 2069, "date": "Feb 2017"},
                        {"site_no_id": 3, "day": 1, "drug_id": "MDMA", "value": 635, "date": "Feb 2017"},
                         {"site_no_id": 2, "day": 2, "drug_id": "MDMA", "value": 714, "date": "Feb 2017"},
                          {"site_no_id": 2, "day": 3, "drug_id": "MDMA", "value": 1355, "date": "Feb 2017"},
                           {"site_no_id": 2, "day": 4, "drug_id": "MDMA", "value": 812, "date": "Feb 2017"},
                            {"site_no_id": 2, "day": 5, "drug_id": "MDMA", "value": 532, "date": "Feb 2017"},
                             {"site_no_id": 2, "day": 6, "drug_id": "MDMA", "value": 123, "date": "Feb 2017"},
                              {"site_no_id": 2, "day": 7, "drug_id": "MDMA", "value": 623, "date": "Feb 2017"},
                              {"site_no_id": 3, "day": 1, "drug_id": "MDMA", "value": 1923, "date": "Mar 2017"},
                               {"site_no_id": 3, "day": 2, "drug_id": "MDMA", "value": 1000, "date": "Mar 2017"},
                                {"site_no_id": 3, "day": 3, "drug_id": "MDMA", "value": 1300, "date": "Mar 2017"},
                                 {"site_no_id": 3, "day": 4, "drug_id": "MDMA", "value": 1000, "date": "Mar 2017"},
                                  {"site_no_id": 3, "day": 5, "drug_id": "MDMA", "value": 1023, "date": "Mar 2017"},
                                   {"site_no_id": 3, "day": 6, "drug_id": "MDMA", "value": 781, "date": "Mar 2017"},
                                    {"site_no_id": 2, "day": 7, "drug_id": "MDMA", "value": 2069, "date": "Mar 2017"},
                                    {"site_no_id": 2, "day": 1, "drug_id": "MDMA", "value": 777, "date": "Mar 2017"},
                                     {"site_no_id": 2, "day": 2, "drug_id": "MDMA", "value": 888, "date": "Mar 2017"},
                                      {"site_no_id": 2, "day": 3, "drug_id": "MDMA", "value": 637, "date": "Mar 2017"},
                                       {"site_no_id": 2, "day": 4, "drug_id": "MDMA", "value": 532, "date": "Mar 2017"},
                                        {"site_no_id": 2, "day": 5, "drug_id": "MDMA", "value": 623, "date": "Mar 2017"},
                                         {"site_no_id": 2, "day": 6, "drug_id": "MDMA", "value": 742, "date": "Mar 2017"},
                                          {"site_no_id": 2, "day": 7, "drug_id": "MDMA", "value": 711, "date": "Mar 2017"},
                                          {"site_no_id": 2, "day": 7, "drug_id": "MDMA", "value": 532, "date": "Apr 2017"},
                                          {"site_no_id": 3, "day": 1, "drug_id": "MDMA", "value": 1923, "date": "Apr 2017"},
                                           {"site_no_id": 3, "day": 2, "drug_id": "MDMA", "value": 523, "date": "Apr 2017"},
                                            {"site_no_id": 3, "day": 3, "drug_id": "MDMA", "value": 1300, "date": "Apr 2017"},
                                             {"site_no_id": 3, "day": 4, "drug_id": "MDMA", "value": 1000, "date": "Apr 2017"},
                                              {"site_no_id": 3, "day": 5, "drug_id": "MDMA", "value": 1023, "date": "Apr 2017"},
                                               {"site_no_id": 3, "day": 6, "drug_id": "MDMA", "value": 781, "date": "Apr 2017"},
                                                {"site_no_id": 2, "day": 7, "drug_id": "MDMA", "value": 2069, "date": "Apr 2017"},
                                                {"site_no_id": 2, "day": 1, "drug_id": "MDMA", "value": 532, "date": "Apr 2017"},
                                                 {"site_no_id": 2, "day": 2, "drug_id": "MDMA", "value": 888, "date": "Apr 2017"},
                                                  {"site_no_id": 2, "day": 3, "drug_id": "MDMA", "value": 637, "date": "Apr 2017"},
                                                   {"site_no_id": 2, "day": 4, "drug_id": "MDMA", "value": 232, "date": "Apr 2017"},
                                                    {"site_no_id": 2, "day": 5, "drug_id": "MDMA", "value": 623, "date": "Apr 2017"},
                                                     {"site_no_id": 2, "day": 6, "drug_id": "MDMA", "value": 742, "date": "Apr 2017"},
                                                      {"site_no_id": 2, "day": 7, "drug_id": "MDMA", "value": 711, "date": "Apr 2017"}]}};
const sampleData2 = [
   {'site_num': '1', 'day': '1', 'drug': 'MDMA', 'value': 80, 'date': 'Apr 2018'},
   {'site_num': '1', 'day': '2', 'drug': 'MDMA', 'value': 70, 'date': 'Apr 2018'},
   {'site_num': '1', 'day': '3', 'drug': 'MDMA', 'value': 68, 'date': 'Apr 2018'},
   {'site_num': '1', 'day': '4', 'drug': 'MDMA', 'value': 56, 'date': 'Apr 2018'},
   {'site_num': '2', 'day': '1', 'drug': 'MDMA', 'value': 80, 'date': 'Apr 2018'},
   {'site_num': '2', 'day': '2', 'drug': 'MDMA', 'value': 70, 'date': 'Apr 2018'},
   {'site_num': '2', 'day': '3', 'drug': 'MDMA', 'value': 68, 'date': 'Apr 2018'},
   {'site_num': '2', 'day': '4', 'drug': 'MDMA', 'value': 56, 'date': 'Apr 2018'},
   {'site_num': '1', 'day': '1', 'drug': 'MDMA', 'value': 76, 'date': 'May 2018'},
   {'site_num': '1', 'day': '2', 'drug': 'MDMA', 'value': 94, 'date': 'May 2018'},
   {'site_num': '1', 'day': '3', 'drug': 'MDMA', 'value': 67, 'date': 'May 2018'},
   {'site_num': '1', 'day': '4', 'drug': 'MDMA', 'value': 87, 'date': 'May 2018'},
   {'site_num': '1', 'day': '5', 'drug': 'MDMA', 'value': 99, 'date': 'May 2018'},
   {'site_num': '2', 'day': '1', 'drug': 'MDMA', 'value': 76, 'date': 'May 2018'},
   {'site_num': '2', 'day': '2', 'drug': 'MDMA', 'value': 94, 'date': 'May 2018'},
   {'site_num': '2', 'day': '3', 'drug': 'MDMA', 'value': 67, 'date': 'May 2018'},
   {'site_num': '2', 'day': '4', 'drug': 'MDMA', 'value': 87, 'date': 'May 2018'},
   {'site_num': '2', 'day': '5', 'drug': 'MDMA', 'value': 99, 'date': 'May 2018'}
];

class visualisations extends Component {
    handleChange = event => {
      this.setState({[event.target.name]: event.target.value }, function() {
          this.getData()
      });
    };

    getData() {
        var self = this
        var drug = this.state.drug
        var sampleDataAdapter = new DataAdapter()
        {/*Send a get request to the drugs API*/}
        const token = sessionStorage.getItem("NangToken");
        axios({
          method: 'get',
          url: 'https://deco3801-nang-it.uqcloud.net/API/Data/?search_type='+drug,
          headers: {
                'Authorization': 'JWT ' + token,
                'Accept' : 'application/json, application/xml',
          }
      }).then(function (response) {
            console.log(response)
            if (response['status'] == 200) {
                self.setState({
                    data: sampleDataAdapter.singleDrugBeforeDate(response["data"]['data'], "Dec 2017")['processedData'],
                    rawData: sampleDataAdapter.singleDrugBeforeDate(response["data"]['data'], "Dec 2017")['rawData']
                })
            } else {
                alert("Sorry, there was an error in the response from the server")
                self.setState({
                    data: sampleDataAdapter.singleDrugBeforeDate(sampleResponse["data"]['data'], "Dec 2017")['processedData'],
                    rawData: sampleDataAdapter.singleDrugBeforeDate(sampleResponse["data"]['data'], "Dec 2017")['rawData']
                })
            }
            return response
        }).catch(function (error) {
            alert("Sorry, there was an error fetching some data from our servers")
            //Default to the backups
            self.setState({
                data: sampleDataAdapter.singleDrugBeforeDate(sampleResponse["data"]['data'], "Dec 2017")['processedData'],
                rawData: sampleDataAdapter.singleDrugBeforeDate(sampleResponse["data"]['data'], "Dec 2017")['rawData']
            })
            return error
        });
    };

    constructor(props) {
      super(props);
      var dataAdapter = new DataAdapter()
      var testData = dataAdapter.singleDrugData(sampleData2, "MDMA")
      // Don't call this.setState() here!
      this.state = {
          drug: 'MDMA',
          data : testData["processedData"],
          rawData : testData["rawData"]
      };
  }

  render() {
      if (this.state.rawData == sampleData2) {
          this.getData()
      }
    return (
      <div>
      <NangNavBar/>
      <div className="container" height="100px">
      <h2> Our latest data</h2>
      <p> Simply select which drug you wish to view below, and we will pull in our latest data! </p>
      <Select
        value={this.state.drug}
        onChange={this.handleChange}
        inputProps={{
          name: 'drug',
          id: 'graph-type-simple',
        }}
      >
        <MenuItem value=''>None</MenuItem>
        <MenuItem value='Amphetamine'>Amphetamine</MenuItem>
        <MenuItem value='Benzoylecgonine'>Benzoylecgonine</MenuItem>
        <MenuItem value='Cotinine'>Cotinine</MenuItem>
        <MenuItem value='Ethanol'>Ethanol</MenuItem>
        <MenuItem value='Hydroxycotinine'>Hydroxycotinine</MenuItem>
        <MenuItem value='JWH-018'>JWH-018</MenuItem>
        <MenuItem value='JWH-073'>JWH-073</MenuItem>
        <MenuItem value='MAM'>MAM</MenuItem>
        <MenuItem value='MDA'>MDA</MenuItem>
        <MenuItem value='MDMA'>MDMA</MenuItem>
        <MenuItem value='Mephedrone'>Mephedrone</MenuItem>
        <MenuItem value='Methamphetamine'>Methamphetamine</MenuItem>
        <MenuItem value='Methylone'>Methylone</MenuItem>
        <MenuItem value='Norfentanyl'>Norfentanyl</MenuItem>
        <MenuItem value='Noroxycodone'>Noroxycodone</MenuItem>
        <MenuItem value='Cannabis'>Cannabis</MenuItem>
      </Select>
      <div style={{paddingTop: '15px'}}>
      <DrugBarGraph title={this.state.data.title} data={this.state.data.data} keys={this.state.data.keys}/>
      </div>

      </div>
      </div>
    )
  }
}

export default visualisations;
