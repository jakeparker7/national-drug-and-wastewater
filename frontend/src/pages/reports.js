import '../App.css';
import React, { Component } from 'react';
import NangNavBar from '../components/NangNavBar';
import pdf from '../nwdmp4.pdf'

class reports extends Component {
  render() {
    return (
      <div>
      <NangNavBar/>
      <div className="container">
          <h2> List of reports </h2>
            <div className="row">
              <div className="col-lg-3">
                <h3> 2018 </h3>
                  <details>
                    <summary>June</summary>
                    <a href={pdf} target="_blank"> June 2018 Report</a>
                    <img src={require('../hpimage1.jpg')} width = "145px" />
                  </details>
                <details>
                  <summary>March</summary>
                  <a href={pdf} target="_blank"> March 2018 Report</a>
                  <img src={require('../hpimage1.jpg')} width = "145px" />
                </details>
              </div>
              <div className="col-lg-3">
                <h3> 2017 </h3>
                  <details>
                    <summary>December</summary>
                    <a href={pdf} target="_blank"> December 2017 Report</a>
                    <img src={require('../hpimage1.jpg')} width = "145px" />
                  </details>
                  <details>
                    <summary>September</summary>
                    <a href={pdf} target="_blank"> September 2017 Report</a>
                    <img src={require('../hpimage1.jpg')} width = "145px" />
                  </details>
                  <details>
                    <summary>June</summary>
                    <a href={pdf} target="_blank"> June 2017 Report </a>
                    <img src={ require('../hpimage1.jpg') } width = "145px" />
                    </details>
                  <details>
                    <summary>March</summary>
                    <a href={pdf} target="_blank"> March 2017 Report</a>
                    <img src={require('../hpimage1.jpg')} width = "145px" />
                  </details>
              </div>
              <div className="col-lg-3">
                <h3> 2016 </h3>
                  <details>
                    <summary>December</summary>
                    <a href={pdf} target="_blank"> December 2016 Report</a>
                    <img src={require('../hpimage1.jpg')} width = "145px" />
                  </details>
                  <details>
                    <summary>September</summary>
                    <a href={pdf} target="_blank"> September 2016 Report</a>
                    <img src={require('../hpimage1.jpg')} width = "145px" />
                  </details>
                  <details>
                    <summary>June</summary>
                    <a href={pdf} target="_blank"> June 2016 Report </a>
                    <img src={require('../hpimage1.jpg')} width = "145px" />
                  </details>
                  <details>
                    <summary>March</summary>
                    <a href={pdf} target="_blank"> March 2016 Report</a>
                    <img src={require('../hpimage1.jpg')} width = "145px" />
                  </details>
              </div>
              <div className="col-lg-3">
                <h3> 2015 </h3>
                  <details>
                    <summary>December</summary>
                    <a href={pdf} target="_blank"> December 2015 Report</a>
                    <img src={require('../hpimage1.jpg')} width = "145px" />
                  </details>
                  <details>
                    <summary>September</summary>
                    <a href={pdf} target="_blank"> September 2015 Report</a>
                    <img src={require('../hpimage1.jpg')} width = "145px" />
                  </details>
                  <details>
                    <summary>June</summary>
                    <a href={pdf} target="_blank"> June 2015 Report </a>
                    <img src={require('../hpimage1.jpg')} width = "145px" />
                  </details>
                  <details>
                    <summary>March</summary>
                    <a href={pdf} target="_blank"> March 2015 Report</a>
                    <img src={require('../hpimage1.jpg')} width = "145px" />
                  </details>
              </div>
          </div>
      </div>
      </div>

    )
  }
}

export default reports;
