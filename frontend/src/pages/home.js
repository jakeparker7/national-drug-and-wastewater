import '../App.css';
import React, { Component } from 'react';
import NangNavBar from '../components/NangNavBar';
import MainFooter from '../components/MainFooter';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import { Link } from 'react-router-dom'

const theme = createMuiTheme({
  palette: {
    primary: blue, // Purple and green play nicely together.
    secondary: { main: '#ffffff' }
  },
});

class HomeContent extends Component {
  render() {
    return (
      <div>
      <NangNavBar/>
      <MuiThemeProvider theme={theme}>
      <div div style={{margin: '5%'}}>
        <div style={{margin:"2%", display: 'flex', justifyContent: 'center'}}>
        <h1>
        National Wastewater Drug Monitoring Program
        </h1>
        </div>

          <div style={{display: "flex", flexDirection:"row"}}>
          <div style={{margin:"1%"}}>
            <Typography variant="headline" color="black">
            About Us
            </Typography>
            <p align="justify">
              The national wastewater drug monitoring program is a research project between the Univeristy of Queensland
              and the Australian Criminal Intelligence commision. The program collects wastewater samples and analyses them
              for drug content - helping to provide a overview of the Australian populations drug usage.
            </p>
            <Link
              className="nav-link"
              to="/about">
              <Button variant="contained" color="primary">
              Learn More
              </Button>
            </Link>
          </div>

          <div style={{margin:"1%"}}>
            <Typography variant="headline" color="black">
            Our data
            </Typography>
            <p align="justify">
              We collect data on the drug consumption of varios metro and regional areas throughout Australia. This data is released to the public through
              reports and graphs - so if you are interested be sure to check them out!
            </p>
            <div style={{display: "flex", flexDirection:"row"}}>
            <Link
              className="nav-link"
              to="/reports">
              <Button variant="contained" color="primary">
              View Reports
              </Button>
            </Link>
            <Link
              className="nav-link"
              to="/visualisations">
              <Button variant="contained" color="primary">
              View Data
              </Button>
            </Link>
            </div>
          </div>
        </div>


      </div>
      <div style={{width: "100%"}}>
        {/** Banner image for bottom of main page **/}
        <div style={{margin:"2%", display: 'flex', justifyContent: 'center'}}>
          <img src={require('../resources/logos.png')} />
        </div>
      </div>
      </MuiThemeProvider>
      </div>
    )
  }
}

export default HomeContent;
