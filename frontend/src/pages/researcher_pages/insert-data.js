import '../../App.css';
import React, { Component } from 'react';
import ResearcherNav from "../../components/ResearcherNav"
import { Redirect } from 'react-router-dom';

class InsertData extends Component {
  render() {
      const token = sessionStorage.getItem("NangToken");
      if (token == "" || token == null) {
          alert("not logged in");
          return (
              <Redirect to="/login"></Redirect>
          )
      } else {
          return (
            <ResearcherNav/>
          )
      }
  }
}

export default InsertData
