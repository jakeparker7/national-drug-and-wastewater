import '../../App.css';
import React, { Component } from 'react';
import ResearcherNav from "../../components/ResearcherNav"
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { Redirect } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import API from '../../components/API'
import axios from 'axios';

const theme = createMuiTheme({
  palette: {
    primary: blue, // Purple and green play nicely together.
    secondary: { main: '#ffffff' }
  },
});

class Admin extends Component {
    state = {
        username:'',
        password:'',
        email:"",
        accounts:[{'value':'Account1','string':"Account1"}, {'value': 'Account2','string': "Account2"}],
        selectedAccount: 'Account1'
    }

    handlePassChange = this.handlePassChange.bind(this);
    handleUserChange = this.handleUserChange.bind(this);
    handleSelectChange = this.handleSelectChange.bind(this);
    handleEmailChange = this.handleEmailChange.bind(this);

    handleUserChange(event) {
        this.setState({
            username: event.target.value,
        });
    };

    handlePassChange(event) {
        this.setState({
            password: event.target.value,
        });
    };

    handleEmailChange(event) {
        this.setState({
            email: event.target.value,
        });
    };

    handleSelectChange(event) {
        this.setState({
            selectedAccount: event.target.value
        })
    }

    handleRegisterUser(event) {
        var self = this
        var this_url = ""
        //Make the api call
        const formData = new FormData();
        formData.append('username',this.state.username)
        formData.append('password',this.state.password)
        formData.append('email', this.state.email)
        const token = sessionStorage.getItem("NangToken");
        axios({
          method: 'post',
          url: 'https://deco3801-nang-it.uqcloud.net/API/HandleUser/',
          headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'JWT ' + token,
                'Accept' : 'application/json,application/xml',
          },
          data:formData
        }).then(function (response) {
            if (response['data'] == "Success") {
                alert("Sucessfully added a user")
            } else {
                alert("Sorry, something went wrong at our severs")
            }
        }).catch(function (error) {
            alert("Sorry, there was an error submitting data")
        });
    }

    getAccounts() {
            return this.state.accounts.map((dt, i) => {
              return (
                <MenuItem
                  key={i}
                  value={dt.value}>
                    {dt.string}
                </MenuItem>
              );
          }
      )
}
  render() {
      const token = sessionStorage.getItem("NangToken");
      if (token == "" || token == null) {
          alert("not logged in");
          return (
              <Redirect to="/login"></Redirect>
          )
      } else {
          return (
              <div>
                <ResearcherNav/>
                <MuiThemeProvider theme={theme}>
                <div style={{margin: '5%'}}>
                <div style={{margin: '2%'}}>
                <div>
                <h1> Register a new user </h1>
                </div>
                  <div>
                      <TextField
                          autoFocus
                          margin="dense"
                          id="user"
                          label="Username"
                          type="text"
                          value={this.state.username}
                          onChange={this.handleUserChange}
                        />
                    </div>
                    <div>
                    <TextField
                          autoFocus
                          margin="normal"
                          id="password"
                          label="Password"
                          type="password"
                          value={this.state.password}
                          onChange={this.handlePassChange}
                        />
                    </div>
                    <div>
                    <TextField
                          autoFocus
                          margin="normal"
                          id="email"
                          label="email"
                          type="email"
                          value={this.state.email}
                          onChange={this.handleEmailChange}
                        />
                    </div>
                    <div>
                    <Button variant="contained" color="primary" onClick={(event) => this.handleRegisterUser(event)}>
                    Add User
                    </Button>
                    </div>
                </div>
                <div style={{margin: '2%'}}>
                <div>
                <h1> Remove a user </h1>
                </div>
                  <div style={{margin:'5px'}}>
                  <Select
                    value={this.state.selectedAccount}
                    onChange={this.handleSelectChange}
                    inputProps={{
                      name: 'age',
                      id: 'age-simple',
                    }}
                  >
                    {this.getAccounts()}
                  </Select>
                    </div>
                    <div>
                    <Button variant="contained" color="primary">
                    Remove User
                    </Button>
                    </div>
                </div>
                </div>
                </MuiThemeProvider>
              </div>
          )
      }
  }
}

export default Admin
