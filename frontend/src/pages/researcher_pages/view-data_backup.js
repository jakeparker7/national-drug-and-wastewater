import '../../App.css';
import React, { Component } from 'react';
import ResearcherNav from "../../components/ResearcherNav";
import DrugBarGraph from "../../components/DrugBarGraph";
import DrugBubbleGraph from "../../components/DrugBubbleGraph";
import DrugHeatMap from "../../components/DrugHeatMap";
import DrugHeatMap2 from "../../components/DrugHeatMap2";
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import RawDataTable from "../../components/RawDataTable"
import { Redirect } from 'react-router-dom';
import DataAdapter from '../../components/DataAdapter'

class ViewData extends Component {
  //Todo we would have a function here that loads in the data for each graph
  //this then gets passed to the DrugBarGraph
  sampleKeys = [
      "QLD",
      "ACT",
      "NT",
      "NSW",
      "SA",
      "TAS",
      "VIC",
      "WA"
  ]

  // barData1 = {
  //   title: "MDMA Monthly",
  //   data:[
  //     {
  //       "month": "JAN",
  //       "QLD": 195,
  //       "QLDColor": "hsl(98, 70%, 50%)",
  //       "ACT": 150,
  //       "ACTColor": "hsl(26, 70%, 50%)",
  //       "NT": 184,
  //       "NTColor": "hsl(101, 70%, 50%)",
  //       "NSW": 184,
  //       "NSWColor": "hsl(56, 70%, 50%)",
  //       "SA": 184,
  //       "SAColor": "hsl(23, 70%, 50%)",
  //       "TAS": 184,
  //       "TASColor": "hsl(160, 70%, 50%)",
  //       "VIC": 184,
  //       "VICColor": "hsl(77, 70%, 50%)",
  //       "WA": 184,
  //       "WAColor": "hsl(35, 70%, 50%)"
  //
  //     },
  //     {
  //       "month": "FEB",
  //       "QLD": 102,
  //       "QLDColor": "hsl(98, 70%, 50%)",
  //       "ACT": 38,
  //       "ACTColor": "hsl(26, 70%, 50%)",
  //       "NT": 250,
  //       "NTColor": "hsl(101, 70%, 50%)",
  //       "NSW": 88,
  //       "NSWColor": "hsl(56, 70%, 50%)",
  //       "SA": 72,
  //       "SAColor": "hsl(23, 70%, 50%)",
  //       "TAS": 105,
  //       "TASColor": "hsl(160, 70%, 50%)",
  //       "VIC": 192,
  //       "VICColor": "hsl(77, 70%, 50%)",
  //       "WA": 122,
  //       "WAColor": "hsl(35, 70%, 50%)"
  //
  //     },
  //     {
  //       "month": "MAR",
  //       "QLD": 102,
  //       "QLDColor": "hsl(98, 70%, 50%)",
  //       "ACT": 38,
  //       "ACTColor": "hsl(26, 70%, 50%)",
  //       "NT": 250,
  //       "NTColor": "hsl(101, 70%, 50%)",
  //       "NSW": 88,
  //       "NSWColor": "hsl(56, 70%, 50%)",
  //       "SA": 72,
  //       "SAColor": "hsl(23, 70%, 50%)",
  //       "TAS": 105,
  //       "TASColor": "hsl(160, 70%, 50%)",
  //       "VIC": 192,
  //       "VICColor": "hsl(77, 70%, 50%)",
  //       "WA": 122,
  //       "WAColor": "hsl(35, 70%, 50%)"
  //
  //     },
  //     {
  //       "month": "APR",
  //       "QLD": 102,
  //       "QLDColor": "hsl(98, 70%, 50%)",
  //       "ACT": 38,
  //       "ACTColor": "hsl(26, 70%, 50%)",
  //       "NT": 250,
  //       "NTColor": "hsl(101, 70%, 50%)",
  //       "NSW": 88,
  //       "NSWColor": "hsl(56, 70%, 50%)",
  //       "SA": 72,
  //       "SAColor": "hsl(23, 70%, 50%)",
  //       "TAS": 105,
  //       "TASColor": "hsl(160, 70%, 50%)",
  //       "VIC": 192,
  //       "VICColor": "hsl(77, 70%, 50%)",
  //       "WA": 122,
  //       "WAColor": "hsl(35, 70%, 50%)"
  //
  //     }]
  // }
  //
  // barData2 = {
  //   title: "Heroin Monthly",
  //   data:[
  //     {
  //       "month": "JAN",
  //       "QLD": 10,
  //       "QLDColor": "hsl(98, 70%, 50%)",
  //       "ACT": 15,
  //       "ACTColor": "hsl(26, 70%, 50%)",
  //       "NT": 14,
  //       "NTColor": "hsl(101, 70%, 50%)",
  //       "NSW": 6,
  //       "NSWColor": "hsl(56, 70%, 50%)",
  //       "SA": 18,
  //       "SAColor": "hsl(23, 70%, 50%)",
  //       "TAS": 2,
  //       "TASColor": "hsl(160, 70%, 50%)",
  //       "VIC": 19,
  //       "VICColor": "hsl(77, 70%, 50%)",
  //       "WA": 13,
  //       "WAColor": "hsl(35, 70%, 50%)"
  //
  //     },
  //     {
  //       "month": "FEB",
  //       "QLD": 13,
  //       "QLDColor": "hsl(98, 70%, 50%)",
  //       "ACT": 16,
  //       "ACTColor": "hsl(26, 70%, 50%)",
  //       "NT": 19,
  //       "NTColor": "hsl(101, 70%, 50%)",
  //       "NSW": 23,
  //       "NSWColor": "hsl(56, 70%, 50%)",
  //       "SA": 32,
  //       "SAColor": "hsl(23, 70%, 50%)",
  //       "TAS": 17,
  //       "TASColor": "hsl(160, 70%, 50%)",
  //       "VIC": 16,
  //       "VICColor": "hsl(77, 70%, 50%)",
  //       "WA": 12,
  //       "WAColor": "hsl(35, 70%, 50%)"
  //
  //     },
  //     {
  //       "month": "MAR",
  //       "QLD": 17,
  //       "QLDColor": "hsl(98, 70%, 50%)",
  //       "ACT": 13,
  //       "ACTColor": "hsl(26, 70%, 50%)",
  //       "NT": 14,
  //       "NTColor": "hsl(101, 70%, 50%)",
  //       "NSW": 14,
  //       "NSWColor": "hsl(56, 70%, 50%)",
  //       "SA": 19,
  //       "SAColor": "hsl(23, 70%, 50%)",
  //       "TAS": 6,
  //       "TASColor": "hsl(160, 70%, 50%)",
  //       "VIC": 2,
  //       "VICColor": "hsl(77, 70%, 50%)",
  //       "WA": 5,
  //       "WAColor": "hsl(35, 70%, 50%)"
  //
  //     },
  //     {
  //       "month": "APR",
  //       "QLD": 18,
  //       "QLDColor": "hsl(98, 70%, 50%)",
  //       "ACT": 22,
  //       "ACTColor": "hsl(26, 70%, 50%)",
  //       "NT": 1,
  //       "NTColor": "hsl(101, 70%, 50%)",
  //       "NSW": 7,
  //       "NSWColor": "hsl(56, 70%, 50%)",
  //       "SA": 3,
  //       "SAColor": "hsl(23, 70%, 50%)",
  //       "TAS": 16,
  //       "TASColor": "hsl(160, 70%, 50%)",
  //       "VIC": 8,
  //       "VICColor": "hsl(77, 70%, 50%)",
  //       "WA": 21,
  //       "WAColor": "hsl(35, 70%, 50%)"
  //
  //     }]
  // }

    bubbleData1 = {
      title: "MDMA Regional/Metro",
      data: {
        "name": "Australia",
        "color": "hsl(20, 70%, 50%)",
        "children": [
          {
            "name": "Queensland",
            "color": "hsl(6, 100%, 50%)",
            "children": [
              {
                "name": "Regional",
                "color": "hsl(100, 70%, 50%)",
                "loc": 2000
              },
              {
                "name": "Metro",
                "color": "hsl(220, 70%, 50%)",
                "loc": 1000
              }
            ]
          },
          {
            "name": "NSW",
            "color": "hsl(60, 100%, 50%)",
            "children": [
              {
                "name": "Regional",
                "color": "hsl(100, 70%, 50%)",
                "loc": 3000
              },
              {
                "name": "Metro",
                "color": "hsl(220, 70%, 50%)",
                "loc": 5000
              }
            ]
          },
          {
            "name": "Western Australia",
            "color": "hsl(60, 100%, 50%)",
            "children": [
              {
                "name": "Regional",
                "color": "hsl(100, 70%, 50%)",
                "loc": 2200
              },
              {
                "name": "Metro",
                "color": "hsl(220, 70%, 50%)",
                "loc": 750
              }
            ]
          }
        ]
      }
    }

    heatMapData = {
    title: "Australian Heatmap",
    data: [
      {
    "y": "0",
    "0": 0,
    "0Color": "hsl(160, 0%, 50%)",
    "1": 0,
    "1Color": "hsl(340, 70%, 50%)",
    "2": 0,
    "2Color": "hsl(275, 70%, 50%)",
    "3": 0,
    "3Color": "hsl(86, 70%, 50%)",
    "4": 0,
    "4Color": "hsl(328, 70%, 50%)",
    "5": 0,
    "5Color": "hsl(13, 70%, 50%)",
    "6": 0,
    "6Color": "hsl(272, 70%, 50%)",
    "7": 0,
    "7Color": "hsl(117, 70%, 50%)",
    "8": 0,
    "8Color": "hsl(45, 70%, 50%)",
    "9": 0,
    "9Color": "hsl(152, 70%, 50%)",
    "10": 0,
    "10Color": "hsl(320, 70%, 50%)",
  },
  {
    "y": "1",
    "0": 0,
    "0Color": "hsl(320, 70%, 50%)",
    "1": 0,
    "1Color": "hsl(340, 70%, 50%)",
    "2": 0,
    "2Color": "hsl(275, 70%, 50%)",
    "3": 0,
    "3Color": "hsl(86, 70%, 50%)",
    "4": 20,
    "4Color": "hsl(328, 70%, 50%)",
    "5": 0,
    "5Color": "hsl(13, 70%, 50%)",
    "6": 0,
    "6Color": "hsl(272, 70%, 50%)",
    "7": 20,
    "7Color": "hsl(117, 70%, 50%)",
    "8": 20,
    "8Color": "hsl(45, 70%, 50%)",
    "9": 0,
    "9Color": "hsl(152, 70%, 50%)",
    "10": 0,
    "10Color": "hsl(320, 70%, 50%)",
  },
  {
    "y": "2",
    "0": 0,
    "0Color": "hsl(320, 70%, 50%)",
    "1": 0,
    "1Color": "hsl(340, 70%, 50%)",
    "2": 20,
    "2Color": "hsl(275, 70%, 50%)",
    "3": 20,
    "3Color": "hsl(86, 70%, 50%)",
    "4": 20,
    "4Color": "hsl(328, 70%, 50%)",
    "5": 20,
    "5Color": "hsl(13, 70%, 50%)",
    "6": 20,
    "6Color": "hsl(272, 70%, 50%)",
    "7": 20,
    "7Color": "hsl(117, 70%, 50%)",
    "8": 20,
    "8Color": "hsl(45, 70%, 50%)",
    "9": 0,
    "9Color": "hsl(152, 70%, 50%)",
    "10": 0,
    "10Color": "hsl(320, 70%, 50%)",
  },
  {
    "y": "3",
    "0": 0,
    "0Color": "hsl(320, 70%, 50%)",
    "1": 20,
    "1Color": "hsl(340, 70%, 50%)",
    "2": 20,
    "2Color": "hsl(275, 70%, 50%)",
    "3": 20,
    "3Color": "hsl(86, 70%, 50%)",
    "4": 20,
    "4Color": "hsl(328, 70%, 50%)",
    "5": 20,
    "5Color": "hsl(13, 70%, 50%)",
    "6": 20,
    "6Color": "hsl(272, 70%, 50%)",
    "7": 20,
    "7Color": "hsl(117, 70%, 50%)",
    "8": 20,
    "8Color": "hsl(45, 70%, 50%)",
    "9": 20,
    "9Color": "hsl(152, 70%, 50%)",
    "10": 0,
    "10Color": "hsl(320, 70%, 50%)",
  },
  {
    "y": "4",
    "0": 0,
    "0Color": "hsl(320, 70%, 50%)",
    "1": 20,
    "1Color": "hsl(340, 70%, 50%)",
    "2": 20,
    "2Color": "hsl(275, 70%, 50%)",
    "3": 20,
    "3Color": "hsl(86, 70%, 50%)",
    "4": 20,
    "4Color": "hsl(328, 70%, 50%)",
    "5": 20,
    "5Color": "hsl(13, 70%, 50%)",
    "6": 20,
    "6Color": "hsl(272, 70%, 50%)",
    "7": 20,
    "7Color": "hsl(117, 70%, 50%)",
    "8": 20,
    "8Color": "hsl(45, 70%, 50%)",
    "9": 20,
    "9Color": "hsl(152, 70%, 50%)",
    "10": 0,
    "10Color": "hsl(320, 70%, 50%)",
  },
  {
    "y": "5",
    "0": 0,
    "0Color": "hsl(320, 70%, 50%)",
    "1": 20,
    "1Color": "hsl(340, 70%, 50%)",
    "2": 20,
    "2Color": "hsl(275, 70%, 50%)",
    "3": 20,
    "3Color": "hsl(86, 70%, 50%)",
    "4": 20,
    "4Color": "hsl(328, 70%, 50%)",
    "5": 0,
    "5Color": "hsl(13, 70%, 50%)",
    "6": 20,
    "6Color": "hsl(272, 70%, 50%)",
    "7": 20,
    "7Color": "hsl(117, 70%, 50%)",
    "8": 20,
    "8Color": "hsl(45, 70%, 50%)",
    "9": 20,
    "9Color": "hsl(152, 70%, 50%)",
    "10": 0,
    "10Color": "hsl(320, 70%, 50%)",
  },
  {
    "y": "6",
    "0": 0,
    "0Color": "hsl(320, 70%, 50%)",
    "1": 20,
    "1Color": "hsl(340, 70%, 50%)",
    "2": 20,
    "2Color": "hsl(275, 70%, 50%)",
    "3": 0,
    "3Color": "hsl(86, 70%, 50%)",
    "4": 0,
    "4Color": "hsl(328, 70%, 50%)",
    "5": 0,
    "5Color": "hsl(13, 70%, 50%)",
    "6": 0,
    "6Color": "hsl(272, 70%, 50%)",
    "7": 0,
    "7Color": "hsl(117, 70%, 50%)",
    "8": 0,
    "8Color": "hsl(45, 70%, 50%)",
    "9": 0,
    "9Color": "hsl(152, 70%, 50%)",
    "10": 0,
    "10Color": "hsl(320, 70%, 50%)",
  },
  {
    "y": "7",
    "0": 0,
    "0Color": "hsl(320, 70%, 50%)",
    "1": 0,
    "1Color": "hsl(340, 70%, 50%)",
    "2": 0,
    "2Color": "hsl(275, 70%, 50%)",
    "3": 0,
    "3Color": "hsl(86, 70%, 50%)",
    "4": 0,
    "4Color": "hsl(328, 70%, 50%)",
    "5": 0,
    "5Color": "hsl(13, 70%, 50%)",
    "6": 0,
    "6Color": "hsl(272, 70%, 50%)",
    "7": 0,
    "7Color": "hsl(117, 70%, 50%)",
    "8": 20,
    "8Color": "hsl(45, 70%, 50%)",
    "9": 0,
    "9Color": "hsl(152, 70%, 50%)",
    "10": 0,
    "10Color": "hsl(320, 70%, 50%)",
  }

]
}

sampleRawData = {
  title: "Queensland January",
  data: [
  {
    "location": "SSS542",
    "date": "3/1/2018",
    "drug": "Cocaine",
    "data": "5"
  },
  {
    "location": "SSS542",
    "date": "3/1/2018",
    "drug": "Heroin",
    "data": "3"
  },
  {
    "location": "SSS542",
    "date": "3/1/2018",
    "drug": "Tobacco",
    "data": "95"
  },
  {
    "location": "SSS542",
    "date": "3/1/2018",
    "drug": "Alcohol",
    "data": "950"
  },
  {
    "location": "SSS542",
    "date": "3/1/2018",
    "drug": "MDMA",
    "data": "9"
  },
  {
    "location": "SSS542",
    "date": "5/1/2018",
    "drug": "Cocaine",
    "data": "8"
  },
  {
    "location": "SSS542",
    "date": "5/1/2018",
    "drug": "Heroin",
    "data": "5"
  },
  {
    "location": "SSS542",
    "date": "5/1/2018",
    "drug": "Tobacco",
    "data": "100"
  },
  {
    "location": "SSS542",
    "date": "5/1/2018",
    "drug": "Alcohol",
    "data": "850"
  },
  {
    "location": "SSS542",
    "date": "5/1/2018",
    "drug": "MDMA",
    "data": "15"
  }
  ]
}


  state = {
    graphType: 'Bar',
    bubbleData: this.bubbleData1,
    barData: this.barData1,
    heatMapData: this.heatMapData,
    rawData: this.sampleRawData,
    sampleKeys: this.sampleKeys
  };

  handleChange = event => {
    this.setState({[event.target.name]: event.target.value });
  };



  render() {
    let graph;
    let data;

    if (this.state.graphType == 'Bar') {
        var sampleDataAdapter = new DataAdapter()
        var barData3 = sampleDataAdapter.singleDrugData()
      graph = <DrugBarGraph title={barData3.title} data={barData3.data} keys={barData3.keys}/>;
      data = <Select
          value={this.state.barData}
          onChange={this.handleChange}
          inputProps={{
            name: 'barData',
            id: 'graph-type-simple',
          }}
        >
          <MenuItem value={this.barData3}>MDMA</MenuItem>
          <MenuItem value={this.barData4}>Heroin</MenuItem>
        </Select>
    } else if (this.state.graphType == "Bubble") {
      graph = <DrugBubbleGraph title={this.state.bubbleData.title} data={this.state.bubbleData.data} keys={this.state.sampleKeys}/>;
      data = <Select
          value={this.state.bubbleData}
          onChange={this.handleChange}
          inputProps={{
            name: 'bubbleData',
            id: 'graph-type-simple',
          }}
        >
          <MenuItem value={this.bubbleData1}>
            <em>MDMA Regional/Metro</em>
          </MenuItem>
        </Select>
    } else if (this.state.graphType == "Heatmap") {
      graph = <DrugHeatMap2 title={this.state.heatMapData.title} data = {this.state.heatMapData.data} keys={this.state.sampleKeys}/>
      data = <Select
          value={this.state.bubbleData}
          onChange={this.handleChange}
          inputProps={{
            name: 'heatData',
            id: 'graph-type-simple',
          }}
        >
          <MenuItem value={this.bubbleData1}>
            <em>Heatmap 1</em>
          </MenuItem>
        </Select>
    } else {
      graph = <Typography variant="headline" align="center" gutterBottom="false" text-align="center">
        Failed to load graph data
      </Typography>
    }

    const token = sessionStorage.getItem("NangToken");
    if (token == "" || token == null) {
        alert("not logged in");
        return (
            <Redirect to="/login"></Redirect>
        )
    } else {
        return (
          <div>
            <ResearcherNav/>
            <div style={{margin: '5%'}}>
            <div style={{margin: '5%'}}>
            <h1 align="center" gutterBottom="false" text-align="center">
              Graphs
            </h1>
            <Select
                value={this.state.graphType}
                onChange={this.handleChange}
                inputProps={{
                  name: 'graphType',
                  id: 'graph-type-simple',
                }}
              >
                <MenuItem value={"Bar"}>
                  <em>Bar</em>
                </MenuItem>
                <MenuItem value={"Bubble"}>Bubble</MenuItem>
                <MenuItem value={"Heatmap"}>Heatmap</MenuItem>
              </Select>
              </div>
              <div style={{margin: '5%'}}>
              <Typography variant="button" align="centre" gutterBottom="false" text-align="centre">
                Graph Data
              </Typography>
              {data}
              </div>
              <Paper elevation={3}>
                <div style={{margin: '5%'}}>
                {graph}
                </div>
              </Paper>

              <h1 align="center" gutterBottom="false" text-align="center">
                Raw Data
              </h1>
              <Select
                  value={this.state.rawData}
                  onChange={this.handleChange}
                  inputProps={{
                    name: 'rawData',
                    id: 'raw-data-simple',
                  }}
                >
                  <MenuItem value={"QLD_Jan"}>
                    <em>QLD January</em>
                  </MenuItem>
                  <MenuItem value={"QLD_Feb"}>QLD Feburary</MenuItem>
                </Select>
                <RawDataTable title={this.sampleRawData.title} data={this.sampleRawData.data}/>
            </div>
          </div>
        )
    }
  }
}

export default ViewData
