import '../../App.css';
import React, { Component } from 'react';
import ResearcherNav from "../../components/ResearcherNav";
import DrugBarGraph from "../../components/DrugBarGraph";
import DrugBubbleGraph from "../../components/DrugBubbleGraph";
import DrugHeatMap from "../../components/DrugHeatMap";
import DrugHeatMap2 from "../../components/DrugHeatMap2";
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import RawDataTable from "../../components/RawDataTable"
import { Redirect } from 'react-router-dom';
import DataAdapter from '../../components/DataAdapter'
import DateMenuItems from '../../components/DateMenuItems'
import axios from 'axios';

const sampleData3 = [{"site_no_id": 3, "day": 1, "drug_id": "Benzoylecgonine", "value": 1718.18812755033, "date": "Oct 2017"},
 {"site_no_id": 3, "day": 2, "drug_id": "Benzoylecgonine", "value": 994.836319629027, "date": "Oct 2017"},
  {"site_no_id": 3, "day": 3, "drug_id": "Benzoylecgonine", "value": 972.877134873415, "date": "Oct 2017"},
   {"site_no_id": 3, "day": 4, "drug_id": "Benzoylecgonine", "value": 800.843624591881, "date": "Oct 2017"},
    {"site_no_id": 3, "day": 5, "drug_id": "Benzoylecgonine", "value": 991.052560231552, "date": "Oct 2017"},
     {"site_no_id": 3, "day": 6, "drug_id": "Benzoylecgonine", "value": 1434.92537596759, "date": "Oct 2017"},
      {"site_no_id": 3, "day": 7, "drug_id": "Benzoylecgonine", "value": 2069.14199398667, "date": "Oct 2017"},
      {"site_no_id": 3, "day": 1, "drug_id": "MDMA", "value": 1718.18812755033, "date": "Oct 2017"},
       {"site_no_id": 3, "day": 2, "drug_id": "MDMA", "value": 994.836319629027, "date": "Oct 2017"},
        {"site_no_id": 3, "day": 3, "drug_id": "MDMA", "value": 972.877134873415, "date": "Oct 2017"},
         {"site_no_id": 3, "day": 4, "drug_id": "MDMA", "value": 800.843624591881, "date": "Oct 2017"},
          {"site_no_id": 3, "day": 5, "drug_id": "MDMA", "value": 991.052560231552, "date": "Oct 2017"},
           {"site_no_id": 3, "day": 6, "drug_id": "MDMA", "value": 1434.92537596759, "date": "Oct 2017"},
            {"site_no_id": 3, "day": 7, "drug_id": "MDMA", "value": 2069.14199398667, "date": "Oct 2017"}];

const sampleResponse = {'data' :{"response": 200,
"data": [{"site_no_id": 3, "day": 1, "drug_id": "Benzoylecgonine", "value": 1718.18812755033, "date": "Oct 2017"},
 {"site_no_id": 3, "day": 2, "drug_id": "Benzoylecgonine", "value": 994.836319629027, "date": "Oct 2017"},
  {"site_no_id": 3, "day": 3, "drug_id": "Benzoylecgonine", "value": 972.877134873415, "date": "Oct 2017"},
   {"site_no_id": 3, "day": 4, "drug_id": "Benzoylecgonine", "value": 800.843624591881, "date": "Oct 2017"},
    {"site_no_id": 3, "day": 5, "drug_id": "Benzoylecgonine", "value": 991.052560231552, "date": "Oct 2017"},
     {"site_no_id": 3, "day": 6, "drug_id": "Benzoylecgonine", "value": 1434.92537596759, "date": "Oct 2017"},
      {"site_no_id": 3, "day": 7, "drug_id": "Benzoylecgonine", "value": 2069.14199398667, "date": "Oct 2017"},
      {"site_no_id": 3, "day": 1, "drug_id": "MDMA", "value": 362.18812755033, "date": "Oct 2017"},
       {"site_no_id": 3, "day": 2, "drug_id": "MDMA", "value": 632.836319629027, "date": "Oct 2017"},
        {"site_no_id": 3, "day": 3, "drug_id": "MDMA", "value": 123.877134873415, "date": "Oct 2017"},
         {"site_no_id": 3, "day": 4, "drug_id": "MDMA", "value": 523.843624591881, "date": "Oct 2017"},
          {"site_no_id": 3, "day": 5, "drug_id": "MDMA", "value": 352.052560231552, "date": "Oct 2017"},
           {"site_no_id": 3, "day": 6, "drug_id": "MDMA", "value": 623.92537596759, "date": "Oct 2017"},
            {"site_no_id": 2, "day": 7, "drug_id": "MDMA", "value": 162.14199398667, "date": "Oct 2017"},
            {"site_no_id": 3, "day": 1, "drug_id": "Benzoylecgonine", "value": 1718.18812755033, "date": "Nov 2017"},
             {"site_no_id": 3, "day": 2, "drug_id": "Benzoylecgonine", "value": 838.836319629027, "date": "Nov 2017"},
              {"site_no_id": 3, "day": 3, "drug_id": "Benzoylecgonine", "value": 562.877134873415, "date": "Nov 2017"},
               {"site_no_id": 3, "day": 4, "drug_id": "Benzoylecgonine", "value": 885.843624591881, "date": "Nov 2017"},
                {"site_no_id": 3, "day": 5, "drug_id": "Benzoylecgonine", "value": 745.052560231552, "date": "Nov 2017"},
                 {"site_no_id": 3, "day": 6, "drug_id": "Benzoylecgonine", "value": 781.92537596759, "date": "Nov 2017"},
                  {"site_no_id": 3, "day": 7, "drug_id": "Benzoylecgonine", "value": 2069.14199398667, "date": "Nov 2017"},
                  {"site_no_id": 3, "day": 1, "drug_id": "MDMA", "value": 352.18812755033, "date": "Nov 2017"},
                   {"site_no_id": 3, "day": 2, "drug_id": "MDMA", "value": 535.836319629027, "date": "Nov 2017"},
                    {"site_no_id": 3, "day": 3, "drug_id": "MDMA", "value": 632.877134873415, "date": "Nov 2017"},
                     {"site_no_id": 3, "day": 4, "drug_id": "MDMA", "value": 342.843624591881, "date": "Nov 2017"},
                      {"site_no_id": 3, "day": 5, "drug_id": "MDMA", "value": 532.052560231552, "date": "Nov 2017"},
                       {"site_no_id": 3, "day": 6, "drug_id": "MDMA", "value": 123.92537596759, "date": "Nov 2017"},
                        {"site_no_id": 2, "day": 7, "drug_id": "MDMA", "value": 623.14199398667, "date": "Nov 2017"},
                        {"site_no_id": 3, "day": 1, "drug_id": "Benzoylecgonine", "value": 1923, "date": "Dec 2017"},
                         {"site_no_id": 3, "day": 2, "drug_id": "Benzoylecgonine", "value": 1000.836319629027, "date": "Dec 2017"},
                          {"site_no_id": 3, "day": 3, "drug_id": "Benzoylecgonine", "value": 1300.877134873415, "date": "Dec 2017"},
                           {"site_no_id": 3, "day": 4, "drug_id": "Benzoylecgonine", "value": 1000.843624591881, "date": "Dec 2017"},
                            {"site_no_id": 3, "day": 5, "drug_id": "Benzoylecgonine", "value": 1023.052560231552, "date": "Dec 2017"},
                             {"site_no_id": 3, "day": 6, "drug_id": "Benzoylecgonine", "value": 781.92537596759, "date": "Dec 2017"},
                              {"site_no_id": 3, "day": 7, "drug_id": "Benzoylecgonine", "value": 2069.14199398667, "date": "Dec 2017"},
                              {"site_no_id": 3, "day": 1, "drug_id": "MDMA", "value": 453.18812755033, "date": "Dec 2017"},
                               {"site_no_id": 3, "day": 2, "drug_id": "MDMA", "value": 634.836319629027, "date": "Dec 2017"},
                                {"site_no_id": 3, "day": 3, "drug_id": "MDMA", "value": 637.877134873415, "date": "Dec 2017"},
                                 {"site_no_id": 3, "day": 4, "drug_id": "MDMA", "value": 532.843624591881, "date": "Dec 2017"},
                                  {"site_no_id": 3, "day": 5, "drug_id": "MDMA", "value": 623.052560231552, "date": "Dec 2017"},
                                   {"site_no_id": 3, "day": 6, "drug_id": "MDMA", "value": 133.92537596759, "date": "Dec 2017"},
                                    {"site_no_id": 2, "day": 7, "drug_id": "MDMA", "value": 711.14199398667, "date": "Dec 2017"}]}};


class ViewData extends Component {

  handleChange = event => {
    this.setState({[event.target.name]: event.target.value }, function() {
        this.getData()
    });
  };
  timeFilterData(response) {
      var sampleDataAdapter = new DataAdapter()
      var count = 0;
      if (this.state.drug1 != "") {
          count = count + 1
      }
      if (this.state.drug2 != "") {
          count = count + 1
      }
      if (this.state.drug3 != "") {
          count = count + 1
      }
      if (count == 1) {
          //Single drug data
          //alert("Single drug processing")
          if (this.state.startDate != "" && this.state.endDate != "") {
              //Get data for between two dates
              //alert("Getting data between " + this.state.startDate + " and " + this.state.endDate)
              this.setState({
                  data: sampleDataAdapter.singleDrugBetweenDate(response["data"]['data'], this.state.startDate, this.state.endDate)['processedData'],
                  rawData: sampleDataAdapter.singleDrugBetweenDate(response["data"]['data'], this.state.startDate, this.state.endDate)['rawData']
              })
          } else if (this.state.startDate != "") {
              //Get all data after a certain date
              //alert("Getting data after " + this.state.startDate)
              this.setState({
                  data: sampleDataAdapter.singleDrugAfterDate(response["data"]['data'], this.state.startDate)['processedData'],
                  rawData: sampleDataAdapter.singleDrugAfterDate(response["data"]['data'], this.state.startDate)['rawData']
              })
          } else if (this.state.endDate != "") {
              //Get all data before a certain date
              //alert("Getting data before " + this.state.endDate)
              this.setState({
                  data: sampleDataAdapter.singleDrugBeforerDate(response["data"]['data'], this.state.endDate)['processedData'],
                  rawData: sampleDataAdapter.singleDrugBeforerDate(response["data"]['data'], this.state.endDate)['rawData']
              })
          } else {
              //Get all data of a certain drug (drug type may be all)
              //alert("Getting all data for " + this.state.drug1 + " " + this.state.drug2 + " " + this.state.drug3)
              this.setState({
                  data: sampleDataAdapter.singleDrugData(response["data"]['data'], this.state.drug)['processedData'],
                  rawData: sampleDataAdapter.singleDrugData(response["data"]['data'], this.state.drug)['rawData']
              })
          }
      } else {
          //Multi drug data
          //alert("Multi drug processing")
          if (this.state.startDate != "" && this.state.endDate != "") {
              //Get data for between two dates
              //alert("Getting data between " + this.state.startDate + " and " + this.state.endDate)
              this.setState({
                  data: sampleDataAdapter.multiDrugBetweenDate(response["data"]['data'], this.state.startDate, this.state.endDate)['processedData'],
                  rawData: sampleDataAdapter.multiDrugBetweenDate(response["data"]['data'], this.state.startDate, this.state.endDate)['rawData']
              })
          } else if (this.state.startDate != "") {
              //Get all data after a certain date
              //alert("Getting data after " + this.state.startDate)
              this.setState({
                  data: sampleDataAdapter.multiDrugAfterData(response["data"]['data'], this.state.startDate)['processedData'],
                  rawData: sampleDataAdapter.multiDrugAfterData(response["data"]['data'], this.state.startDate)['rawData']
              })
          } else if (this.state.endDate != "") {
              //Get all data before a certain date
              //alert("Getting data before " + this.state.endDate)
              this.setState({
                  data: sampleDataAdapter.multiDrugBeforeData(response["data"]['data'], this.state.endDate)['processedData'],
                  rawData: sampleDataAdapter.multiDrugBeforeData(response["data"]['data'], this.state.endDate)['rawData']
              })
          } else {
              //Get all data of a certain drug (drug type may be all)
              //alert("Getting all data for " + this.state.drug1 + " " + this.state.drug2 + " " + this.state.drug3)
              this.setState({
                  data: sampleDataAdapter.multiDrugData(response["data"]['data'])['processedData'],
                  rawData: sampleDataAdapter.multiDrugData(response["data"]['data'])['rawData']
              })
          }
      }
  }
  getData(dataNum) {
      //Get the needed drug data
      var search_param = ""
      if (this.state.drug1 == "" && this.state.drug2 == "" && this.state.drug3 == "") {
          search_param = search_param.concat("all")
      } else {
          if (this.state.drug1 != "") {
              search_param = search_param.concat(this.state.drug1)
          }
          if (this.state.drug2 != "") {
              search_param = search_param.concat("_".concat(this.state.drug2))
          }
          if (this.state.drug3 != "") {
              search_param = search_param.concat("_".concat(this.state.drug3))
          }
      }
      var self = this
      var sampleDataAdapter = new DataAdapter()
      {/*Send a get request to the drugs API*/}
      const token = sessionStorage.getItem("NangToken");
      axios({
        method: 'get',
        url: 'https://deco3801-nang-it.uqcloud.net/API/Data/?search_type='+search_param,
        headers: {
              'Authorization': 'JWT ' + token,
              'Accept' : 'application/json,application/xml',
        }
    }).then(function (response) {
        console.log(response)
        if (response['status'] == 200) {
            // self.setState({
            //     data: sampleDataAdapter.singleDrugData(response["data"]['data'], self.state.drug)['processedData'],
            //     rawData: sampleDataAdapter.singleDrugData(response["data"]['data'], self.state.drug)['rawData']
            // })
            self.timeFilterData(response)
        } else {
            alert("Sorry, there was an error in the response from the server")
            // self.setState({
            //     data: sampleDataAdapter.multiDrugData(sampleResponse["data"], self.state.drug)['processedData'],
            //     rawData: sampleDataAdapter.multiDrugData(sampleResponse["data"], self.state.drug)['rawData']
            // })
            self.timeFilterData(sampleResponse)
        }
          return response
      }).catch(function (error) {
          alert("Sorry, there was an error fetching some data from our servers")
          //Default to the backups
          self.timeFilterData(sampleResponse)
          // self.setState({
          //     data: sampleDataAdapter.multiDrugData(sampleResponse["data"], self.state.drug)['processedData'],
          //     rawData: sampleDataAdapter.multiDrugData(sampleResponse["data"], self.state.drug)['rawData']
          // })
          return error
      });
  };

  constructor(props) {
      super(props);
      var dataAdapter = new DataAdapter
      var testData = dataAdapter.singleDrugData(sampleData3)
      // Set initial state with sample data
      this.state = {
          graphType: 'Bar',
          drug1: 'MDMA',
          drug2: '',
          drug3: '',
          startDate: "",
          endDate: "",
          data : testData["processedData"],
          rawData : testData["rawData"],
      };
  }



  render() {
    let graph
    let data

    //Get data if we need to
    if (this.state.rawData == sampleData3) {
        this.getData()
    }
    if (this.state.graphType == 'Bar') {
        //Make the request for data from the data adapter
        graph = <DrugBarGraph title={this.state.data.title} data={this.state.data.data} keys={this.state.data.keys}/>;
        data =
          <div>
            <Select
          value={this.state.drug1}
          onChange={this.handleChange}
          inputProps={{
            name: 'drug1',
            id: 'graph-type-simple',
          }}
        >
          <MenuItem value=''>None</MenuItem>
          <MenuItem value='Amphetamine'>Amphetamine</MenuItem>
          <MenuItem value='Benzoylecgonine'>Benzoylecgonine</MenuItem>
          <MenuItem value='Cotinine'>Cotinine</MenuItem>
          <MenuItem value='Ethanol'>Ethanol</MenuItem>
          <MenuItem value='Hydroxycotinine'>Hydroxycotinine</MenuItem>
          <MenuItem value='JWH-018'>JWH-018</MenuItem>
          <MenuItem value='JWH-073'>JWH-073</MenuItem>
          <MenuItem value='MAM'>MAM</MenuItem>
          <MenuItem value='MDA'>MDA</MenuItem>
          <MenuItem value='MDMA'>MDMA</MenuItem>
          <MenuItem value='Mephedrone'>Mephedrone</MenuItem>
          <MenuItem value='Methamphetamine'>Methamphetamine</MenuItem>
          <MenuItem value='Methylone'>Methylone</MenuItem>
          <MenuItem value='Norfentanyl'>Norfentanyl</MenuItem>
          <MenuItem value='Noroxycodone'>Noroxycodone</MenuItem>
          <MenuItem value='Cannabis'>Cannabis</MenuItem>
        </Select>
        <Select
          value={this.state.drug2}
          onChange={this.handleChange}
          inputProps={{
            name: 'drug2',
            id: 'graph-type-simple',
          }}
        >
          <MenuItem value=''>None</MenuItem>
          <MenuItem value='Amphetamine'>Amphetamine</MenuItem>
          <MenuItem value='Benzoylecgonine'>Benzoylecgonine</MenuItem>
          <MenuItem value='Cotinine'>Cotinine</MenuItem>
          <MenuItem value='Ethanol'>Ethanol</MenuItem>
          <MenuItem value='Hydroxycotinine'>Hydroxycotinine</MenuItem>
          <MenuItem value='JWH-018'>JWH-018</MenuItem>
          <MenuItem value='JWH-073'>JWH-073</MenuItem>
          <MenuItem value='MAM'>MAM</MenuItem>
          <MenuItem value='MDA'>MDA</MenuItem>
          <MenuItem value='MDMA'>MDMA</MenuItem>
          <MenuItem value='Mephedrone'>Mephedrone</MenuItem>
          <MenuItem value='Methamphetamine'>Methamphetamine</MenuItem>
          <MenuItem value='Methylone'>Methylone</MenuItem>
          <MenuItem value='Norfentanyl'>Norfentanyl</MenuItem>
          <MenuItem value='Noroxycodone'>Noroxycodone</MenuItem>
          <MenuItem value='Cannabis'>Cannabis</MenuItem>
        </Select>
        <Select
          value={this.state.drug3}
          onChange={this.handleChange}
          inputProps={{
            name: 'drug3',
            id: 'graph-type-simple',
          }}
        >
          <MenuItem value=''>None</MenuItem>
          <MenuItem value='Amphetamine'>Amphetamine</MenuItem>
          <MenuItem value='Benzoylecgonine'>Benzoylecgonine</MenuItem>
          <MenuItem value='Cotinine'>Cotinine</MenuItem>
          <MenuItem value='Ethanol'>Ethanol</MenuItem>
          <MenuItem value='Hydroxycotinine'>Hydroxycotinine</MenuItem>
          <MenuItem value='JWH-018'>JWH-018</MenuItem>
          <MenuItem value='JWH-073'>JWH-073</MenuItem>
          <MenuItem value='MAM'>MAM</MenuItem>
          <MenuItem value='MDA'>MDA</MenuItem>
          <MenuItem value='MDMA'>MDMA</MenuItem>
          <MenuItem value='Mephedrone'>Mephedrone</MenuItem>
          <MenuItem value='Methamphetamine'>Methamphetamine</MenuItem>
          <MenuItem value='Methylone'>Methylone</MenuItem>
          <MenuItem value='Norfentanyl'>Norfentanyl</MenuItem>
          <MenuItem value='Noroxycodone'>Noroxycodone</MenuItem>
          <MenuItem value='Cannabis'>Cannabis</MenuItem>
        </Select>
        </div>
    } else {
      graph = <Typography variant="headline" align="center" gutterBottom="false" text-align="center">
        Failed to load graph data
      </Typography>
    }

    const token = sessionStorage.getItem("NangToken")
    if (token == "" || token == null) {
        alert("not logged in")
        return (
            <Redirect to="/login"></Redirect>
        )
    } else {
        return (
          <div>
            <ResearcherNav/>
            <div style={{margin: '5%'}}>
            <div style={{margin: '5%'}}>
            <h1 align="center" gutterBottom="false" text-align="center">
              Graphs
            </h1>
            <Select
                value={this.state.graphType}
                onChange={this.handleChange}
                inputProps={{
                  name: 'graphType',
                  id: 'graph-type-simple',
                }}
              >
                <MenuItem value={"Bar"}>
                  <em>Bar</em>
                </MenuItem>
                <MenuItem value={"Bubble"}>Bubble</MenuItem>
                <MenuItem value={"Heatmap"}>Heatmap</MenuItem>
              </Select>
              </div>
              <div style={{margin: '5%'}}>
              <Typography variant="button" align="centre" gutterBottom="false" text-align="centre">
                Start Date
              </Typography>
              <Select
                  value={this.state.startDate}
                  onChange={this.handleChange}
                  inputProps={{
                    name: 'startDate'
                  }}
                >
                //Date Menu Items
                <MenuItem value={""}>
                  <em>None</em>
                </MenuItem>
                    // 2016 //
                <MenuItem value={"Jan 2016"}>
                  <em>Jan 2016</em>
                </MenuItem>
                <MenuItem value={"Feb 2016"}>
                  <em>Feb 2016</em>
                </MenuItem>
                <MenuItem value={"Mar 2016"}>
                  <em>Mar 2016</em>
                </MenuItem>
                <MenuItem value={"Apr 2016"}>
                  <em>Apr 2016</em>
                </MenuItem>
                <MenuItem value={"May 2016"}>
                  <em>May 2016</em>
                </MenuItem>
                <MenuItem value={"Jun 2016"}>
                  <em>Jun 2016</em>
                </MenuItem>
                <MenuItem value={"Jul 2016"}>
                  <em>Jul 2016</em>
                </MenuItem>
                <MenuItem value={"Aug 2016"}>
                  <em>Aug 2016</em>
                </MenuItem>
                <MenuItem value={"Sep 2016"}>
                  <em>Sep 2016</em>
                </MenuItem>
                <MenuItem value={"Oct 2016"}>
                  <em>Oct 2016</em>
                </MenuItem>
                <MenuItem value={"Nov 2016"}>
                  <em>Nov 2016</em>
                </MenuItem>
                <MenuItem value={"Dec 2016"}>
                  <em>Dec 2016</em>
                </MenuItem>
                    // 2017 //
                <MenuItem value={"Jan 2017"}>
                  <em>Jan 2017</em>
                </MenuItem>
                <MenuItem value={"Feb 2017"}>
                  <em>Feb 2017</em>
                </MenuItem>
                <MenuItem value={"Mar 2017"}>
                  <em>Mar 2017</em>
                </MenuItem>
                <MenuItem value={"Apr 2017"}>
                  <em>Apr 2017</em>
                </MenuItem>
                <MenuItem value={"May 2017"}>
                  <em>May 2017</em>
                </MenuItem>
                <MenuItem value={"Jun 2017"}>
                  <em>Jun 2017</em>
                </MenuItem>
                <MenuItem value={"Jul 2017"}>
                  <em>Jul 2017</em>
                </MenuItem>
                <MenuItem value={"Aug 2017"}>
                  <em>Aug 2017</em>
                </MenuItem>
                <MenuItem value={"Sep 2017"}>
                  <em>Sep 2017</em>
                </MenuItem>
                <MenuItem value={"Oct 2017"}>
                  <em>Oct 2017</em>
                </MenuItem>
                <MenuItem value={"Nov 2017"}>
                  <em>Nov 2017</em>
                </MenuItem>
                <MenuItem value={"Dec 2017"}>
                  <em>Dec 2017</em>
                </MenuItem>
                    // 2018 //
                <MenuItem value={"Jan 2018"}>
                  <em>Jan 2018</em>
                </MenuItem>
                <MenuItem value={"Feb 2018"}>
                  <em>Feb 2018</em>
                </MenuItem>
                <MenuItem value={"Mar 2018"}>
                  <em>Mar 2018</em>
                </MenuItem>
                <MenuItem value={"Apr 2018"}>
                  <em>Apr 2018</em>
                </MenuItem>
                <MenuItem value={"May 2018"}>
                  <em>May 2018</em>
                </MenuItem>
                <MenuItem value={"Jun 2018"}>
                  <em>Jun 2018</em>
                </MenuItem>
                <MenuItem value={"Jul 2018"}>
                  <em>Jul 2018</em>
                </MenuItem>
                <MenuItem value={"Aug 2018"}>
                  <em>Aug 2018</em>
                </MenuItem>
                <MenuItem value={"Sep 2018"}>
                  <em>Sep 2018</em>
                </MenuItem>
                <MenuItem value={"Oct 2018"}>
                  <em>Oct 2018</em>
                </MenuItem>
                <MenuItem value={"Nov 2018"}>
                  <em>Nov 2018</em>
                </MenuItem>
                <MenuItem value={"Dec 2018"}>
                  <em>Dec 2018</em>
                </MenuItem>
                    // 2019 //
                <MenuItem value={"Jan 2019"}>
                  <em>Jan 2019</em>
                </MenuItem>
                <MenuItem value={"Feb 2019"}>
                  <em>Feb 2019</em>
                </MenuItem>
                <MenuItem value={"Mar 2019"}>
                  <em>Mar 2019</em>
                </MenuItem>
                <MenuItem value={"Apr 2019"}>
                  <em>Apr 2019</em>
                </MenuItem>
                <MenuItem value={"May 2019"}>
                  <em>May 2019</em>
                </MenuItem>
                <MenuItem value={"Jun 2019"}>
                  <em>Jun 2019</em>
                </MenuItem>
                <MenuItem value={"Jul 2019"}>
                  <em>Jul 2019</em>
                </MenuItem>
                <MenuItem value={"Aug 2019"}>
                  <em>Aug 2019</em>
                </MenuItem>
                <MenuItem value={"Sep 2019"}>
                  <em>Sep 2019</em>
                </MenuItem>
                <MenuItem value={"Oct 2019"}>
                  <em>Oct 2019</em>
                </MenuItem>
                <MenuItem value={"Nov 2019"}>
                  <em>Nov 2019</em>
                </MenuItem>
                <MenuItem value={"Dec 2018"}>
                  <em>Dec 2019</em>
                </MenuItem>
                    // 2020 //
                <MenuItem value={"Jan 2020"}>
                  <em>Jan 2020</em>
                </MenuItem>
                <MenuItem value={"Feb 2020"}>
                  <em>Feb 2020</em>
                </MenuItem>
                <MenuItem value={"Mar 2020"}>
                  <em>Mar 2020</em>
                </MenuItem>
                <MenuItem value={"Apr 2020"}>
                  <em>Apr 2020</em>
                </MenuItem>
                <MenuItem value={"May 2020"}>
                  <em>May 2020</em>
                </MenuItem>
                <MenuItem value={"Jun 2020"}>
                  <em>Jun 2020</em>
                </MenuItem>
                <MenuItem value={"Jul 2020"}>
                  <em>Jul 2020</em>
                </MenuItem>
                <MenuItem value={"Aug 2020"}>
                  <em>Aug 2020</em>
                </MenuItem>
                <MenuItem value={"Sep 2020"}>
                  <em>Sep 2020</em>
                </MenuItem>
                <MenuItem value={"Oct 2020"}>
                  <em>Oct 2020</em>
                </MenuItem>
                <MenuItem value={"Nov 2020"}>
                  <em>Nov 2020</em>
                </MenuItem>
                <MenuItem value={"Dec 2020"}>
                  <em>Dec 2020</em>
                </MenuItem>
                </Select>
                </div>
                <div style={{margin: '5%'}}>
                <Typography variant="button" align="centre" gutterBottom="false" text-align="centre">
                  End Date
                </Typography>
                <Select
                    value={this.state.endDate}
                    onChange={this.handleChange}
                    inputProps={{
                      name: 'endDate'
                    }}
                  >
                  <MenuItem value={""}>
                    <em>None</em>
                  </MenuItem>
                      // 2016 //
                  <MenuItem value={"Jan 2016"}>
                    <em>Jan 2016</em>
                  </MenuItem>
                  <MenuItem value={"Feb 2016"}>
                    <em>Feb 2016</em>
                  </MenuItem>
                  <MenuItem value={"Mar 2016"}>
                    <em>Mar 2016</em>
                  </MenuItem>
                  <MenuItem value={"Apr 2016"}>
                    <em>Apr 2016</em>
                  </MenuItem>
                  <MenuItem value={"May 2016"}>
                    <em>May 2016</em>
                  </MenuItem>
                  <MenuItem value={"Jun 2016"}>
                    <em>Jun 2016</em>
                  </MenuItem>
                  <MenuItem value={"Jul 2016"}>
                    <em>Jul 2016</em>
                  </MenuItem>
                  <MenuItem value={"Aug 2016"}>
                    <em>Aug 2016</em>
                  </MenuItem>
                  <MenuItem value={"Sep 2016"}>
                    <em>Sep 2016</em>
                  </MenuItem>
                  <MenuItem value={"Oct 2016"}>
                    <em>Oct 2016</em>
                  </MenuItem>
                  <MenuItem value={"Nov 2016"}>
                    <em>Nov 2016</em>
                  </MenuItem>
                  <MenuItem value={"Dec 2016"}>
                    <em>Dec 2016</em>
                  </MenuItem>
                      // 2017 //
                  <MenuItem value={"Jan 2017"}>
                    <em>Jan 2017</em>
                  </MenuItem>
                  <MenuItem value={"Feb 2017"}>
                    <em>Feb 2017</em>
                  </MenuItem>
                  <MenuItem value={"Mar 2017"}>
                    <em>Mar 2017</em>
                  </MenuItem>
                  <MenuItem value={"Apr 2017"}>
                    <em>Apr 2017</em>
                  </MenuItem>
                  <MenuItem value={"May 2017"}>
                    <em>May 2017</em>
                  </MenuItem>
                  <MenuItem value={"Jun 2017"}>
                    <em>Jun 2017</em>
                  </MenuItem>
                  <MenuItem value={"Jul 2017"}>
                    <em>Jul 2017</em>
                  </MenuItem>
                  <MenuItem value={"Aug 2017"}>
                    <em>Aug 2017</em>
                  </MenuItem>
                  <MenuItem value={"Sep 2017"}>
                    <em>Sep 2017</em>
                  </MenuItem>
                  <MenuItem value={"Oct 2017"}>
                    <em>Oct 2017</em>
                  </MenuItem>
                  <MenuItem value={"Nov 2017"}>
                    <em>Nov 2017</em>
                  </MenuItem>
                  <MenuItem value={"Dec 2017"}>
                    <em>Dec 2017</em>
                  </MenuItem>
                      // 2018 //
                  <MenuItem value={"Jan 2018"}>
                    <em>Jan 2018</em>
                  </MenuItem>
                  <MenuItem value={"Feb 2018"}>
                    <em>Feb 2018</em>
                  </MenuItem>
                  <MenuItem value={"Mar 2018"}>
                    <em>Mar 2018</em>
                  </MenuItem>
                  <MenuItem value={"Apr 2018"}>
                    <em>Apr 2018</em>
                  </MenuItem>
                  <MenuItem value={"May 2018"}>
                    <em>May 2018</em>
                  </MenuItem>
                  <MenuItem value={"Jun 2018"}>
                    <em>Jun 2018</em>
                  </MenuItem>
                  <MenuItem value={"Jul 2018"}>
                    <em>Jul 2018</em>
                  </MenuItem>
                  <MenuItem value={"Aug 2018"}>
                    <em>Aug 2018</em>
                  </MenuItem>
                  <MenuItem value={"Sep 2018"}>
                    <em>Sep 2018</em>
                  </MenuItem>
                  <MenuItem value={"Oct 2018"}>
                    <em>Oct 2018</em>
                  </MenuItem>
                  <MenuItem value={"Nov 2018"}>
                    <em>Nov 2018</em>
                  </MenuItem>
                  <MenuItem value={"Dec 2018"}>
                    <em>Dec 2018</em>
                  </MenuItem>
                      // 2019 //
                  <MenuItem value={"Jan 2019"}>
                    <em>Jan 2019</em>
                  </MenuItem>
                  <MenuItem value={"Feb 2019"}>
                    <em>Feb 2019</em>
                  </MenuItem>
                  <MenuItem value={"Mar 2019"}>
                    <em>Mar 2019</em>
                  </MenuItem>
                  <MenuItem value={"Apr 2019"}>
                    <em>Apr 2019</em>
                  </MenuItem>
                  <MenuItem value={"May 2019"}>
                    <em>May 2019</em>
                  </MenuItem>
                  <MenuItem value={"Jun 2019"}>
                    <em>Jun 2019</em>
                  </MenuItem>
                  <MenuItem value={"Jul 2019"}>
                    <em>Jul 2019</em>
                  </MenuItem>
                  <MenuItem value={"Aug 2019"}>
                    <em>Aug 2019</em>
                  </MenuItem>
                  <MenuItem value={"Sep 2019"}>
                    <em>Sep 2019</em>
                  </MenuItem>
                  <MenuItem value={"Oct 2019"}>
                    <em>Oct 2019</em>
                  </MenuItem>
                  <MenuItem value={"Nov 2019"}>
                    <em>Nov 2019</em>
                  </MenuItem>
                  <MenuItem value={"Dec 2018"}>
                    <em>Dec 2019</em>
                  </MenuItem>
                      // 2020 //
                  <MenuItem value={"Jan 2020"}>
                    <em>Jan 2020</em>
                  </MenuItem>
                  <MenuItem value={"Feb 2020"}>
                    <em>Feb 2020</em>
                  </MenuItem>
                  <MenuItem value={"Mar 2020"}>
                    <em>Mar 2020</em>
                  </MenuItem>
                  <MenuItem value={"Apr 2020"}>
                    <em>Apr 2020</em>
                  </MenuItem>
                  <MenuItem value={"May 2020"}>
                    <em>May 2020</em>
                  </MenuItem>
                  <MenuItem value={"Jun 2020"}>
                    <em>Jun 2020</em>
                  </MenuItem>
                  <MenuItem value={"Jul 2020"}>
                    <em>Jul 2020</em>
                  </MenuItem>
                  <MenuItem value={"Aug 2020"}>
                    <em>Aug 2020</em>
                  </MenuItem>
                  <MenuItem value={"Sep 2020"}>
                    <em>Sep 2020</em>
                  </MenuItem>
                  <MenuItem value={"Oct 2020"}>
                    <em>Oct 2020</em>
                  </MenuItem>
                  <MenuItem value={"Nov 2020"}>
                    <em>Nov 2020</em>
                  </MenuItem>
                  <MenuItem value={"Dec 2020"}>
                    <em>Dec 2020</em>
                  </MenuItem>
                  </Select>
                  </div>
              <div style={{margin: '5%'}}>
              <Typography variant="button" align="centre" gutterBottom="false" text-align="centre">
                Drugs to display
              </Typography>
              {data}
              </div>
              <Paper elevation={3}>
                <div style={{margin: '5%'}}>
                {graph}
                </div>
              </Paper>

              <h1 align="center" gutterBottom="false" text-align="center">
                Raw Data
              </h1>
                <RawDataTable title='Sample Raw Data' data={this.state.rawData}/>
            </div>
          </div>
        )
    }
  }
}

export default ViewData
