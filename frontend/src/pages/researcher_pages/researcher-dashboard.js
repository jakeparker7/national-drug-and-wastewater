import '../../App.css';
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import ResearcherNav from "../../components/ResearcherNav"
import MessageDisplay from "../../components/MessageDisplay"
import SplitterLayout from 'react-splitter-layout';
import Paper from '@material-ui/core/Paper';
import DrugBarGraph from "../../components/DrugBarGraph"
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DataAdapter from '../../components/DataAdapter'
import API from '../../components/API'
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import blue from '@material-ui/core/colors/blue';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';


const theme = createMuiTheme({
  palette: {
    primary: blue, // Purple and green play nicely together.
    secondary: { main: '#ffffff' }
  },
});

const sampleMessageData = [
  {
    "date_time": "3:52pm",
    "author": "Ben Tscharke",
    "message": "I think I have fixed the issue on 007 Jan 2017 Day 2 - have a look please"
  },
  {
    "date_time": "1:23pm",
    "author": "Jake O'Brien",
    "message": "May be a problem with data from site 007 on Jan 2017 Day 2"
}];

const sampleChangelogData = [
  {
    "date_time": "3:50pm",
    "author": "Ben Tscharke",
    "message": "Added data 001_Apr 2016_Day 1"
  },
  {
    "date_time": "9:58am",
    "author": "Jake O'Brien",
    "message": "Added data 001_Apr 2016_Day 2"
}];

const sampleData3 = [{"site_no_id": 3, "day": 1, "drug_id": "MDMA", "value": 1718.18812755033, "date": "Oct 2017"},
 {"site_no_id": 3, "day": 2, "drug_id": "MDMA", "value": 994.836319629027, "date": "Oct 2017"},
  {"site_no_id": 3, "day": 3, "drug_id": "MDMA", "value": 972.877134873415, "date": "Oct 2017"},
   {"site_no_id": 3, "day": 4, "drug_id": "MDMA", "value": 800.843624591881, "date": "Oct 2017"},
    {"site_no_id": 3, "day": 5, "drug_id": "MDMA", "value": 991.052560231552, "date": "Oct 2017"},
     {"site_no_id": 3, "day": 6, "drug_id": "MDMA", "value": 1434.92537596759, "date": "Oct 2017"},
      {"site_no_id": 3, "day": 7, "drug_id": "MDMA", "value": 2069.14199398667, "date": "Oct 2017"}]

      const sampleResponse = {'data' :{"response": 200,
      "data": [{"site_no_id": 3, "day": 1, "drug_id": "Benzoylecgonine", "value": 1718, "date": "Oct 2017"},
       {"site_no_id": 3, "day": 2, "drug_id": "Benzoylecgonine", "value": 994, "date": "Oct 2017"},
        {"site_no_id": 3, "day": 3, "drug_id": "Benzoylecgonine", "value": 972, "date": "Oct 2017"},
         {"site_no_id": 3, "day": 4, "drug_id": "Benzoylecgonine", "value": 800, "date": "Oct 2017"},
          {"site_no_id": 3, "day": 5, "drug_id": "Benzoylecgonine", "value": 991, "date": "Oct 2017"},
           {"site_no_id": 3, "day": 6, "drug_id": "Benzoylecgonine", "value": 1434, "date": "Oct 2017"},
            {"site_no_id": 2, "day": 7, "drug_id": "Benzoylecgonine", "value": 2069, "date": "Oct 2017"},
            {"site_no_id": 2, "day": 1, "drug_id": "Benzoylecgonine", "value": 859, "date": "Oct 2017"},
             {"site_no_id": 2, "day": 2, "drug_id": "Benzoylecgonine", "value": 920, "date": "Oct 2017"},
              {"site_no_id": 2, "day": 3, "drug_id": "Benzoylecgonine", "value": 1233, "date": "Oct 2017"},
               {"site_no_id": 2, "day": 4, "drug_id": "Benzoylecgonine", "value": 674, "date": "Oct 2017"},
                {"site_no_id": 2, "day": 5, "drug_id": "Benzoylecgonine", "value": 723, "date": "Oct 2017"},
                 {"site_no_id": 2, "day": 6, "drug_id": "Benzoylecgonine", "value": 623, "date": "Oct 2017"},
                  {"site_no_id": 2, "day": 7, "drug_id": "Benzoylecgonine", "value": 646, "date": "Oct 2017"},
                  {"site_no_id": 3, "day": 1, "drug_id": "Benzoylecgonine", "value": 1718, "date": "Nov 2017"},
                   {"site_no_id": 3, "day": 2, "drug_id": "Benzoylecgonine", "value": 838, "date": "Nov 2017"},
                    {"site_no_id": 3, "day": 3, "drug_id": "Benzoylecgonine", "value": 562, "date": "Nov 2017"},
                     {"site_no_id": 3, "day": 4, "drug_id": "Benzoylecgonine", "value": 885, "date": "Nov 2017"},
                      {"site_no_id": 3, "day": 5, "drug_id": "Benzoylecgonine", "value": 745, "date": "Nov 2017"},
                       {"site_no_id": 3, "day": 6, "drug_id": "Benzoylecgonine", "value": 781, "date": "Nov 2017"},
                        {"site_no_id": 3, "day": 7, "drug_id": "Benzoylecgonine", "value": 2069, "date": "Nov 2017"},
                        {"site_no_id": 3, "day": 1, "drug_id": "Benzoylecgonine", "value": 635, "date": "Nov 2017"},
                         {"site_no_id": 2, "day": 2, "drug_id": "Benzoylecgonine", "value": 714, "date": "Nov 2017"},
                          {"site_no_id": 2, "day": 3, "drug_id": "Benzoylecgonine", "value": 1355, "date": "Nov 2017"},
                           {"site_no_id": 2, "day": 4, "drug_id": "Benzoylecgonine", "value": 812, "date": "Nov 2017"},
                            {"site_no_id": 2, "day": 5, "drug_id": "Benzoylecgonine", "value": 532, "date": "Nov 2017"},
                             {"site_no_id": 2, "day": 6, "drug_id": "Benzoylecgonine", "value": 123, "date": "Nov 2017"},
                              {"site_no_id": 2, "day": 7, "drug_id": "Benzoylecgonine", "value": 623, "date": "Nov 2017"},
                              {"site_no_id": 3, "day": 1, "drug_id": "Benzoylecgonine", "value": 1923, "date": "Dec 2017"},
                               {"site_no_id": 3, "day": 2, "drug_id": "Benzoylecgonine", "value": 1000, "date": "Dec 2017"},
                                {"site_no_id": 3, "day": 3, "drug_id": "Benzoylecgonine", "value": 1300, "date": "Dec 2017"},
                                 {"site_no_id": 3, "day": 4, "drug_id": "Benzoylecgonine", "value": 1000, "date": "Dec 2017"},
                                  {"site_no_id": 3, "day": 5, "drug_id": "Benzoylecgonine", "value": 1023, "date": "Dec 2017"},
                                   {"site_no_id": 3, "day": 6, "drug_id": "Benzoylecgonine", "value": 781, "date": "Dec 2017"},
                                    {"site_no_id": 2, "day": 7, "drug_id": "Benzoylecgonine", "value": 2069, "date": "Dec 2017"},
                                    {"site_no_id": 2, "day": 1, "drug_id": "Benzoylecgonine", "value": 777, "date": "Dec 2017"},
                                     {"site_no_id": 2, "day": 2, "drug_id": "Benzoylecgonine", "value": 888, "date": "Dec 2017"},
                                      {"site_no_id": 2, "day": 3, "drug_id": "Benzoylecgonine", "value": 637, "date": "Dec 2017"},
                                       {"site_no_id": 2, "day": 4, "drug_id": "Benzoylecgonine", "value": 532, "date": "Dec 2017"},
                                        {"site_no_id": 2, "day": 5, "drug_id": "Benzoylecgonine", "value": 623, "date": "Dec 2017"},
                                         {"site_no_id": 2, "day": 6, "drug_id": "Benzoylecgonine", "value": 742, "date": "Dec 2017"},
                                          {"site_no_id": 2, "day": 7, "drug_id": "Benzoylecgonine", "value": 711, "date": "Dec 2017"}]}};

class ResearcherDashboard extends Component {
    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleNameChange = event => {
        this.setState({ name: event.target.value });
    };

    handleMessageChange = event => {
        this.setState({ message: event.target.value });
    };

    handleMessageSubmit() {
        alert(this.state.name + " " + this.state.message + " " + this.state.messageType)
        this.setState({open: false})
        var self = this
        var this_url = ""
        if (this.state.messageType == "messagelog") {
            this_url = "https://deco3801-nang-it.uqcloud.net/API/MessageData/"
        } else {
            this_url = "https://deco3801-nang-it.uqcloud.net/API/ChangeLogData/"
        }
        //Make the api call
        const formData = new FormData();
        formData.append('name',this.state.name)
        formData.append('message',this.state.message)
        const token = sessionStorage.getItem("NangToken");
        axios({
          method: 'post',
          url: this_url,
          headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'JWT ' + token,
                'Accept' : 'application/json,application/xml',
          },
          data:formData
        }).then(function (response) {
            if (response['data']['response'] == "Success") {
                alert("Sucessfully submitted a message or changelog")
            } else {
                alert("Sorry, something went wrong at our severs")
            }
        }).catch(function (error) {
            alert("Sorry, there was an error submitting data")
        });
    }

    handleClose = () => {
        this.setState({ open: false });
    };

    handleChange = event => {
      this.setState({[event.target.name]: event.target.value })
    };


    getChangelogData() {
        var self = this
        var sampleDataAdapter = new DataAdapter()
        {/*Send a get request to the drugs API*/}
        const token = sessionStorage.getItem("NangToken");
        axios({
          method: 'get',
          url: 'https://deco3801-nang-it.uqcloud.net/API/ChangeLogData/',
          headers: {
                'Authorization': 'JWT ' + token,
                'Accept' : 'application/json, application/xml',
          }
        }).then(function (response) {
            console.log(response)
            if (response['status'] == 200 && response['data']['data'].length != 0) {
                self.setState({
                    changelogData: response['data']['data']
                })
            } else {
                alert("Sorry, there was an error in the response from the server")
                self.setState({
                    changelogData: sampleChangelogData
                })
            }
            return response
        }).catch(function (error) {
            alert("Sorry, there was an error fetching some data from our servers 1")
            //Default to the backups
            self.setState({
                changelogData: sampleChangelogData
            })
            return error
        });
    }

    getMessageData() {
        var self = this
        var sampleDataAdapter = new DataAdapter()
        {/*Send a get request to the drugs API*/}
        const token = sessionStorage.getItem("NangToken");
        axios({
          method: 'get',
          url: 'https://deco3801-nang-it.uqcloud.net/API/MessageData/',
          headers: {
                'Authorization': 'JWT ' + token,
                'Accept' : 'application/json, application/xml',
          }
        }).then(function (response) {
            console.log(response)
            if (response['status'] == 200 && response['data']['data'].length != 0) {
                self.setState({
                    messageData: response['data']['data']
                })
            } else {
                alert("Sorry, there was an error in the response from the server")
                self.setState({
                    messageData: sampleMessageData
                })
            }
            return response
        }).catch(function (error) {
            alert("Sorry, there was an error fetching some data from our servers 2")
            //Default to the backups
            self.setState({
                messageData: sampleMessageData
            })
            return error
        });
    }


    getData() {
        var self = this
        var sampleDataAdapter = new DataAdapter()
        {/*Send a get request to the drugs API*/}
        const token = sessionStorage.getItem("NangToken");
        axios({
          method: 'get',
          url: 'https://deco3801-nang-it.uqcloud.net/API/Data/?search_type=MDMA',
          headers: {
                'Authorization': 'JWT ' + token,
                'Accept' : 'application/json, application/xml',
          }
      }).then(function (response) {
            console.log(response)
            if (response['status'] == 200) {
                self.setState({
                    data: sampleDataAdapter.singleDrugData(response["data"]['data'])['processedData'],
                    rawData: sampleDataAdapter.singleDrugData(response["data"]['data'])['rawData']
                })
            } else {
                alert("Sorry, there was an error in the response from the server")
                self.setState({
                    data: sampleDataAdapter.singleDrugData(sampleResponse["data"]['data'])['processedData'],
                    rawData: sampleDataAdapter.singleDrugData(sampleResponse["data"]['data'])['rawData']
                })
            }
            return response
        }).catch(function (error) {
            alert("Sorry, there was an error fetching some data from our servers")
            //Default to the backups
            self.setState({
                data: sampleDataAdapter.singleDrugData(sampleData3)['processedData'],
                rawData: sampleDataAdapter.singleDrugData(sampleData3)['rawData']
            })
            return error
        });
    };

    constructor(props) {
      super(props);
      var dataAdapter = new DataAdapter
      var testData = dataAdapter.singleDrugData(sampleResponse['data']['data'])
      // Don't call this.setState() here!
      this.state = {
          drug: '',
          data : testData["processedData"],
          rawData : testData["rawData"],
          messageData: sampleMessageData,
          changelogData: sampleChangelogData,
          open: false,
          messageType: "changelog",
          name: "",
          message: ""
      };
      this.getChangelogData()
      this.getMessageData()

      //this.testSetData()
  }

  render() {
      const token = sessionStorage.getItem("NangToken")
      var sampleDataAdapter = new DataAdapter()
      // this.state.data = sampleDataAdapter.singleDrugData(this.state.drug)['processedData']
      // this.state.rawData = sampleDataAdapter.singleDrugData(this.state.drug)['rawData']
      var api = new API()
      var sampleMessageData = api.getMessageData()
      var sampleChangelogData = api.getChangelogData()
      //Get data from the server if we are still showing the sample data
      if (this.state.rawData == sampleResponse['data']['data']) {
          this.getData()
      }
      if (token == "" || token == null) {
          alert("not logged in")
          return (
              <Redirect to="/login"></Redirect>
          )
      } else {
          return (
            <div>
            <MuiThemeProvider theme={theme}>
            <ResearcherNav/>
            <SplitterLayout>
              <div style={{margin: '10%'}}>
                <h1> Summary of latest graph </h1>
                <DrugBarGraph title={this.state.data.title} data={this.state.data.data} keys={this.state.data.keys}/>
              </div>

              <div>

                <div style={{margin: '10%'}}>
                  <Paper>
                    <div style={{margin: '5% 5% 5% 5%'}}>
                    <MessageDisplay title = "Changelog" data = {this.state.changelogData}/>
                    </div>
                  </Paper>
                </div>

                <div style={{margin: '10%'}}>
                  <Paper>
                    <div style={{margin: '5% 5% 5% 5%'}}>
                    <MessageDisplay title = "Message Board" data = {this.state.messageData}/>
                    </div>
                  </Paper>

                  <Button variant="contained" color="primary" onClick={() => this.setState({open:true})}>
                    Add a new message or change
                    </Button>
                    <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="form-dialog-title"
                    >
                    <DialogTitle id="form-dialog-title">Register your interest</DialogTitle>
                    <DialogContent>
                    <DialogContentText>
                      To get news on when the NWDMP website and portal is released, please
                      provide your email below.
                    </DialogContentText>
                    <TextField
                      autoFocus
                      margin="dense"
                      id="name"
                      label="Name"
                      type="email"
                      fullWidth
                      onChange={this.handleNameChange}
                    />
                    <TextField
                      autoFocus
                      margin="dense"
                      id="name"
                      label="Message"
                      type="email"
                      fullWidth
                      onChange={this.handleMessageChange}
                    />
                    <Select
                      value={this.state.messageType}
                      onChange={this.handleChange}
                      inputProps={{
                        name: 'messageType',
                        id: 'graph-type-simple',
                      }}
                    >
                      <MenuItem value='changelog'>Changelog</MenuItem>
                      <MenuItem value='messagelog'>Messagelog</MenuItem>
                    </Select>
                  </DialogContent>
                    <DialogActions>
                    <Button onClick={this.handleClose} color="primary">
                      Cancel
                    </Button>
                    <Button onClick={() => this.handleMessageSubmit()} color="primary">
                      Submit
                    </Button>
                    </DialogActions>
                    </Dialog>
                </div>

              </div>
            </SplitterLayout>
            </MuiThemeProvider>
            </div>
          )
      }
  }
}

export default ResearcherDashboard
