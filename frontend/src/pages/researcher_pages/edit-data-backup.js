import '../../App.css';
import React, { Component } from 'react';
import { FilePicker } from 'react-file-picker'
import ResearcherNav from "../../components/ResearcherNav"
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import { Redirect } from 'react-router-dom';

const theme = createMuiTheme({
  palette: {
    primary: blue, // Purple and green play nicely together.
    secondary: { main: '#ffffff' }
  },
});

class EditData extends Component {
  state = {
    open: false,
    vertical: 'bottom',
    horizontal: 'right',
    message: "Processing data",
    site: "",
    drug: "",
    batch: "",
    concentration: ""
  };

  handleChangeConcentraion = event => {
    this.setState({ open: true, message: "Processing data"});
  };

  handleChangePopulation = event => {
    this.setState({ open: true, message: "Processing data"});
  };

  handleChangeFlow = event => {
    this.setState({ open: true, message: "Processing data"});
  };

  handleError = event => {
    this.setState({ open: true, message: "Data submission cancelled"});
  };

  handleClick = state => () => {
    this.setState({ open: true});
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { vertical, horizontal, open, message } = this.state;
    const token = sessionStorage.getItem("NangToken");
    if (token == "" || token == null) {
        alert("not logged in");
        return (
            <Redirect to="/login"></Redirect>
        )
    } else {
        return (
          <div>
          <MuiThemeProvider theme={theme}>
          <ResearcherNav/>
          <div style={{margin:"2%"}}>

          <div style={{margin:"1%"}}>
          <h2>
            Import data from CSV
          </h2>
                <div style={{margin:'5px'}}>
                      <FilePicker
                        extensions={['csv']}
                        onChange={this.handleChangeConcentraion}
                        onError={this.handleError}
                        >
                          <Button variant="contained" color="primary">
                            Add Concentrations Data
                          </Button>
                      </FilePicker>
                  </div>
                  <div style={{margin:'5px'}}>
                      <FilePicker
                        extensions={['csv']}
                        onChange={this.handleChangePopulation}
                        onError={this.handleError}
                        >
                            <Button variant="contained" color="primary">
                                Update Populations Data
                            </Button>
                      </FilePicker>
                  </div>
                  <div style={{margin:'5px'}}>
                      <FilePicker
                        extensions={['csv']}
                        onChange={this.handleChangeFlow}
                        onError={this.handleError}
                        >
                            <Button variant="contained" color="primary">
                                Update Flows Data
                            </Button>
                      </FilePicker>
                  </div>
            </div>

          <div style={{margin:"1%"}}>
            <h2>
              Enter new data
            </h2>
            <TextField
                autoFocus
                margin="dense"
                id="site"
                label="Site"
                type="text"
                onChange={(event,newValue) => this.setState({site:newValue})}
              />
              <TextField
                  autoFocus
                  margin="dense"
                  id="drug"
                  label="Drug"
                  type="text"
                  onChange={(event,newValue) => this.setState({drug:newValue})}
                />
                <TextField
                    autoFocus
                    margin="dense"
                    id="batch-date"
                    label="Batch Date"
                    type="text"
                    onChange={(event,newValue) => this.setState({batch:newValue})}
                  />
                <TextField
                    autoFocus
                    margin="dense"
                    id="concentration"
                    label="Concentration"
                    type="text"
                    onChange={(event,newValue) => this.setState({concentration:newValue})}
                  />
                  <FilePicker
                    extensions={['csv']}
                    onChange={this.handleChange}
                    onError={this.handleError}
                    >
                      <Button variant="contained" color="primary">
                      Add data
                      </Button>
                  </FilePicker>
            </div>

              <div style={{margin:"1%"}}>
                <h2>
                  Edit data
                </h2>
                <TextField
                    autoFocus
                    margin="dense"
                    id="site"
                    label="Site"
                    type="text"
                    onChange={(event,newValue) => this.setState({site:newValue})}
                  />
                  <TextField
                      autoFocus
                      margin="dense"
                      id="drug"
                      label="Drug"
                      type="text"
                      onChange={(event,newValue) => this.setState({drug:newValue})}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        id="batch-date"
                        label="Batch Date"
                        type="text"
                        onChange={(event,newValue) => this.setState({batch:newValue})}
                      />
                    <TextField
                        autoFocus
                        margin="dense"
                        id="concentration"
                        label="Concentration"
                        type="text"
                        onChange={(event,newValue) => this.setState({concentration:newValue})}
                      />
                      <div>
                        <Button variant="contained" color="primary">
                        Edit Data
                        </Button>
                      </div>
                </div>

                <div style={{margin:"1%"}}>
                  <h2>
                    Remove data
                  </h2>
                  <TextField
                      autoFocus
                      margin="dense"
                      id="site"
                      label="Site"
                      type="text"
                      onChange={(event,newValue) => this.setState({site:newValue})}
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        id="drug"
                        label="Drug"
                        type="text"
                        onChange={(event,newValue) => this.setState({drug:newValue})}
                      />
                      <TextField
                          autoFocus
                          margin="dense"
                          id="batch-date"
                          label="Batch Date"
                          type="text"
                          onChange={(event,newValue) => this.setState({batch:newValue})}
                        />
                      <TextField
                          autoFocus
                          margin="dense"
                          id="concentration"
                          label="Concentration"
                          type="text"
                          onChange={(event,newValue) => this.setState({concentration:newValue})}
                        />
                        <div>
                          <Button variant="contained" color="primary">
                          Remove Data
                          </Button>
                        </div>
                  </div>
          </div>
          <Snackbar
              anchorOrigin={{ vertical, horizontal }}
              open={open}
              onClose={this.handleClose}
              ContentProps={{
                'aria-describedby': 'message-id',
              }}
              message={<span id="message-id">{message}</span>}
              autoHideDuration={6000}
              action={[
                <IconButton
                  key="close"
                  aria-label="Close"
                  color="inherit"
                  onClick={this.handleClose}
                >
                  <CloseIcon />
                </IconButton>,
              ]}
            />
          </MuiThemeProvider>
          </div>
        )
    }
  }
}

export default EditData
