import '../../App.css';
import React, { Component } from 'react';
import { FilePicker } from 'react-file-picker'
import ResearcherNav from "../../components/ResearcherNav"
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

const theme = createMuiTheme({
  palette: {
    primary: blue, // Purple and green play nicely together.
    secondary: { main: '#ffffff' }
  },
});

class EditData extends Component {
  state = {
    open: false,
    vertical: 'bottom',
    horizontal: 'right',
    message: "Processing data",
    site: "",
    drug: "",
    batch: "",
    concentration: ""
  };

  handleChangeConcentraion(csvFile) {
    this.setState({ open: true, message: "Processing concentration data"});
    var self = this
    //Make the API call
    const token = sessionStorage.getItem("NangToken");
    const formData = new FormData();
    formData.append('csv_file',csvFile)
    axios({
      method: 'post',
      url: 'https://deco3801-nang-it.uqcloud.net/API/Data/',
      headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': 'JWT ' + token,
            'Accept' : 'application/json,application/xml',
      },
      data: formData
      // data: {
      //     csv_file : csvFile
      // }
    }).then(function (response) {
        if (response == "Success") {
          self.setState({message: "Data sucessfully submitted"})
        } else {
            self.setState({message: "Sorry, there was an error submitting data: " + response})
        }
    }).catch(function (error) {
        self.setState({message: "Sorry, there was an error submitting data"})
    });
  };

  handleChangePopulation(csvFile) {
    this.setState({ open: true, message: "Processing population data"});
    var self = this
    //Make the API call
    const token = sessionStorage.getItem("NangToken");
    const formData = new FormData();
    formData.append('csv_file',csvFile)
    axios({
      method: 'post',
      url: 'https://deco3801-nang-it.uqcloud.net/API/Site/',
      headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': 'JWT ' + token,
            'Accept' : 'application/json,application/xml',
      },
      data: formData
  }).then(function (response) {
      if (response == "Success") {
          self.setState({message: "Data sucessfully submitted"})
      } else {
          self.setState({message: "Sorry, there was an error submitting data: " + response})
      }
    }).catch(function (error) {
        self.setState({message: "Sorry, there was an error connecting to our servers"})
    });
  };

  handleChangeFlow(csvFile) {
    this.setState({ open: true, message: "Processing flow data"});
    var self = this
    //Make the API call
    const token = sessionStorage.getItem("NangToken");
    const formData = new FormData();
    formData.append('csv_file',csvFile)
    axios({
      method: 'post',
      url: 'https://deco3801-nang-it.uqcloud.net/API/Flows/',
      headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': 'JWT ' + token,
            'Accept' : 'application/json,application/xml',
      },
      data: formData
      // data: {
      //     csv_file : csvFile
      // }
  }).then(function (response) {
      if (response == "Success") {
          self.setState({message: "Data sucessfully submitted"})
      } else {
          self.setState({message: "Sorry, there was an error submitting data: " + response})
      }
    }).catch(function (error) {
        self.setState({message: "Sorry, there was an error connecting to our servers"})
    });
  };

  handleChangeDrugConstants(csvFile) {
    this.setState({ open: true, message: "Processing flow data"});
    var self = this
    //Make the API call
    const token = sessionStorage.getItem("NangToken");
    const formData = new FormData();
    formData.append('csv_file',csvFile)
    axios({
      method: 'post',
      url: 'https://deco3801-nang-it.uqcloud.net/API/Drug/',
      headers: {
            'Content-Type': 'multipart/form-data',
            'Authorization': 'JWT ' + token,
            'Accept' : 'application/json,application/xml',
      },
      data: formData
      // data: {
      //     csv_file : csvFile
      // }
  }).then(function (response) {
        if (response == "Success") {
            self.setState({message: "Data sucessfully submitted"})
        } else {
            self.setState({message: "Sorry, there was an error submitting data: " + response})
        }
    }).catch(function (error) {
        self.setState({message: "Sorry, there was an error connecting to our servers"})
    });
  };

  handleError = event => {
    this.setState({ open: true, message: "Data submission cancelled"});
  };

  handleClick = state => () => {
    this.setState({ open: true});
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { vertical, horizontal, open, message } = this.state;
    const token = sessionStorage.getItem("NangToken");
    if (token == "" || token == null) {
        alert("not logged in");
        return (
            <Redirect to="/login"></Redirect>
        )
    } else {
        return (
          <div>
          <MuiThemeProvider theme={theme}>
          <ResearcherNav/>
          <div style={{margin:"2%"}}>

          <div style={{margin:"1%"}}>
          <h2>
            Import data from CSV
          </h2>
          <p> To import data into the system, please select one of the options below. Note that concentration data must follow the standard
            concentration data styling. Each entry must be in the form month year, drug, samplesite_month year_day, concentration. Any entrys
            in the csv file not in this format will not be added. The following is an example of an accepted data entry:
            </p>
            <p>
            Aug 2016, Amphetamine, 009_Aug 2016_Day 1,	99.1
            </p>
          <div style={{display: 'flex', justifyContent: 'left'}}>
                <div style={{margin:'5px'}}>
                      <FilePicker
                        extensions={['csv']}
                        onChange={FileObject => {
                            this.handleChangeConcentraion(FileObject)
                            }
                        }
                        onError={this.handleError}
                        >
                          <Button variant="contained" color="primary">
                            Add Concentrations Data
                          </Button>
                      </FilePicker>
                  </div>
                  <div style={{margin:'5px'}}>
                      <FilePicker
                        extensions={['csv']}
                        onChange={FileObject => {
                            this.handleChangePopulation(FileObject)
                            }
                        }
                        onError={this.handleError}
                        >
                            <Button variant="contained" color="primary">
                                Update Populations Data
                            </Button>
                      </FilePicker>
                  </div>
                  <div style={{margin:'5px'}}>
                      <FilePicker
                        extensions={['csv']}
                        onChange={FileObject => {
                            this.handleChangeFlow(FileObject)
                            }
                        }
                        onError={this.handleError}
                        >
                            <Button variant="contained" color="primary">
                                Update Flows Data
                            </Button>
                      </FilePicker>
                  </div>
                  <div style={{margin:'5px'}}>
                      <FilePicker
                        extensions={['csv']}
                        onChange={FileObject => {
                            this.handleChangeDrugConstants(FileObject)
                            }
                        }
                        onError={this.handleError}
                        >
                            <Button variant="contained" color="primary">
                                Update Drug constants
                            </Button>
                      </FilePicker>
                  </div>
            </div>
            </div>
          </div>
          <Snackbar
              anchorOrigin={{ vertical, horizontal }}
              open={open}
              onClose={this.handleClose}
              ContentProps={{
                'aria-describedby': 'message-id',
              }}
              message={<span id="message-id">{message}</span>}
              autoHideDuration={6000}
              action={[
                <IconButton
                  key="close"
                  aria-label="Close"
                  color="inherit"
                  onClick={this.handleClose}
                >
                  <CloseIcon />
                </IconButton>,
              ]}
            />
          </MuiThemeProvider>
          </div>
        )
    }
  }
}

export default EditData
