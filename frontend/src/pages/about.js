import '../App.css';
import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import Paper from '@material-ui/core/Paper'
import NangNavBar from '../components/NangNavBar';
import SvgIcon from '@material-ui/core/SvgIcon';
import FacebookBox from 'mdi-material-ui/FacebookBox'
import TwitterBox from 'mdi-material-ui/TwitterBox'


class about extends Component {
  render() {
    return (
      <div>
      <NangNavBar/>
      <div div style={{margin:"5%", display: 'flex', justifyContent: 'center'}}>
      <div style={{display: "flex", flexDirection:"row"}}>
        <div style={{margin:"2%"}}>
          <h2> About Us</h2>
          <p> Level 4, 20 Cornwall Street  Woolloongabba QLD 4102 </p>
          <p> P: +61 7 3443 2443 </p>
          <p> qaehsadmin@uq.edu.au </p>
          <p>Find us on social media @UQhealth</p>
          <div>
            <Link
              to="/facebook">
              <FacebookBox/>
            </Link>

            <Link
              to="/twitter">
              <TwitterBox/>
            </Link>
          </div>
        </div>

        <div style={{margin:"2%"}}>
          <h2> Our Researchers</h2>
          <h3>
            Dr Ben Tscharke
          </h3>
          <p> Postdoctoral Research Fellow </p>
          <p> Queensland Alliance for Environmental Health Science </p>
          <p> Faculty of Health and Behavioural Sciences</p>

          <h3>
            Dr Jake OBrien
          </h3>
          <p> Postdoctoral Research Fellow </p>
          <p> Queensland Alliance for Environmental Health Science </p>
          <p> Faculty of Health and Behavioural Sciences</p>
        <div>
      </div>
      </div>
      </div>
      </div>
      </div>
    )
  }
}

export default about;
