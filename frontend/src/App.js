import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

import logo from './logo.svg';
import './App.css';

import NangNavBar from './components/NangNavBar';
import home from './pages/home'
import reports from './pages/reports'
import visualisations from './pages/visualisations'
import about from './pages/about'
import login from "./pages/login"
import admin from "./pages/researcher_pages/admin"
import editData from "./pages/researcher_pages/edit-data"
import viewData from "./pages/researcher_pages/view-data"
import dashboard from "./pages/researcher_pages/researcher-dashboard"
import stats from "./pages/stats"

class App extends Component {
    constructor() {
        super();
    }

    componentDidMount(){
    document.title = "NWDMP"
    }

    render() {
        return (
        <div>
              <Route exact path="/" component={home} />
              <Route path="/reports" component={reports} />
              <Route path="/visualisations" component={visualisations} />
              <Route path="/about" component={about} />
              <Route path="/login" component={login} />
              <Route exact path="/researcher-portal" component={dashboard}/>
              <Route path="/researcher-portal/dashboard" component={dashboard}/>
              <Route path="/researcher-portal/edit-data" component={editData}/>
              <Route path="/researcher-portal/view-data" component={viewData}/>
              <Route path="/researcher-portal/admin" component={admin}/>
              <Route exact path="/facebook" component={() => window.location = 'https://www.facebook.com/UQHealth'} />
              <Route exact path="/twitter" component={() => window.location = 'https://www.facebook.com/UQHealth'} />
              <Route exact path="/facebook-share" component={() => window.location = 'https://www.facebook.com/sharer/sharer.php?u=https%3A//deco3801-nang-it.uqcloud.net/product-website'} />
              <Route exact path="/twitter-share" component={() => window.location = 'https://twitter.com/home?status=Check%20out%20the%20new%20Australian%20National%20Wasterwater%20Drug%20Monitoring%20Program%20(NWDMP)%20website!%0Ahttps%3A//deco3801-nang-it.uqcloud.net/product-website'} />
              <Route exact path="/googleplus-share" component={() => window.location = 'https://plus.google.com/share?url=https%3A//deco3801-nang-it.uqcloud.net/product-website'} />
              <Route exact path="/auth" component={() => window.location = 'https://deco3801-nang-it.uqcloud.net/uqauth/index.php'} />
          </div>
        );
      }
  }

export default App;
