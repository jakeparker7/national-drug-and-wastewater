import '../App.css';
import React, { Component } from 'react';
import { ResponsiveBar } from '@nivo/bar'

class DrugBarGraph extends Component {

  render() {
    return (
      <div>
        <h2> {this.props.title} </h2>
        <div style={{ height: 400 }}>
        <ResponsiveBar
          data = {this.props.data}
          keys={this.props.keys}
          indexBy="month"
          margin={{
              "top": 50,
              "right": 130,
              "bottom": 50,
              "left": 60
          }}
          padding={0.3}
          colors="nivo"
          colorBy="id"
          defs={[
              {
                  "id": "dots",
                  "type": "patternDots",
                  "background": "inherit",
                  "color": "#38bcb2",
                  "size": 4,
                  "padding": 1,
                  "stagger": true
              },
              {
                  "id": "lines",
                  "type": "patternLines",
                  "background": "inherit",
                  "color": "#eed312",
                  "rotation": -45,
                  "lineWidth": 6,
                  "spacing": 10
              }
          ]}
          fill={[
              {
                  "match": {
                      "id": "fries"
                  },
                  "id": "dots"
              },
              {
                  "match": {
                      "id": "sandwich"
                  },
                  "id": "lines"
              }
          ]}
          borderColor="inherit:darker(1.6)"
          axisBottom={{
              "orient": "bottom",
              "tickSize": 5,
              "tickPadding": 5,
              "tickRotation": 0,
              "legend": "Month",
              "legendPosition": "center",
              "legendOffset": 36
          }}
          axisLeft={{
              "orient": "left",
              "tickSize": 5,
              "tickPadding": 5,
              "tickRotation": 0,
              "legend": "concentration",
              "legendPosition": "center",
              "legendOffset": -40
          }}
          groupMode = "grouped"
          labelSkipWidth={12}
          labelSkipHeight={12}
          labelTextColor="inherit:darker(1.6)"
          animate={true}
          motionStiffness={90}
          motionDamping={15}
          legends={[
              {
                  "dataFrom": "keys",
                  "anchor": "bottom-right",
                  "direction": "column",
                  "translateX": 120,
                  "itemWidth": 100,
                  "itemHeight": 20,
                  "itemsSpacing": 2,
                  "symbolSize": 20
              }
          ]}
          theme={{
              "tooltip": {
                  "container": {
                      "fontSize": "13px"
                  }
              },
              "labels": {
                  "textColor": "#555"
              }
          }}
          />
        </div>
      </div>
    )
  }
}

export default DrugBarGraph;
