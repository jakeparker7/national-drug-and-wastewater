import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';


// let id = 0;
// function createData(time, author, message) {
//   id += 1;
//   return { time, author, message };
// }
//
// const rows = [
//   createData('4:20pm', "Ray", "its time"),
//   createData('4:20pm', "Ray", "its time"),
//   createData('4:20pm', "Ray", "its time"),
// ];
/** Componenet for displaying messages and logging changes on the researcher dashboard */

class MessageDisplay extends Component {

  render() {
    {/* data passed in is an array of rows with an id, author and message */}
    const rows = this.props.data
    return(
      <div>
      <h1> {this.props.title} </h1>
      <Table>

      {/* Table header */}
      <TableHead>
          <TableRow>
            <TableCell>Time</TableCell>
            <TableCell>Author</TableCell>
            <TableCell>Message</TableCell>
          </TableRow>
        </TableHead>

      {/* Map the data into the table */}
      {rows.map(row => {
            return (
              <TableRow key={row.id}>
                <TableCell component="th" scope="row">
                  {row.date_time}
                </TableCell>
                <TableCell>{row.author}</TableCell>
                <TableCell>{row.message}</TableCell>
              </TableRow>
            );
          })}
      </Table>
      </div>
    )
  }
}


export default MessageDisplay;
