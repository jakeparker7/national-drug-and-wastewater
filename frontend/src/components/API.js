import axios from 'axios';
import DataAdapter from './DataAdapter'

const sampleData = [
    {'date': 'Apr 2018', 'batch': '001_Apr2018_Day1', 'concentration': 130},
    {'date': 'Apr 2018', 'batch': '001_Apr2018_Day2', 'concentration': 160},
    {'date': 'Apr 2018', 'batch': '001_Apr2018_Day3', 'concentration': 150},
    {'date': 'Apr 2018', 'batch': '002_Apr2018_Day1', 'concentration': 130},
    {'date': 'Apr 2018', 'batch': '005_Apr2018_Day2', 'concentration': 160},
    {'date': 'May 2018', 'batch': '006_Apr2018_Day2', 'concentration': 150},
    {'date': 'May 2018', 'batch': '001_Apr2018_Day1', 'concentration': 130},
    {'date': 'May 2018', 'batch': '001_Apr2018_Day2', 'concentration': 160},
    {'date': 'May 2018', 'batch': '002_Apr2018_Day2', 'concentration': 150},
    {'date': 'May 2018', 'batch': '005_Apr2018_Day1', 'concentration': 130},
    {'date': 'May 2018', 'batch': '006_Apr2018_Day2', 'concentration': 160},
    {'date': 'May 2018', 'batch': '007_Apr2018_Day2', 'concentration': 150}
];

const sampleData2 = [
    {'date': 'Apr 2018', 'batch': '001_Apr2018_Day1', 'concentration': 5},
    {'date': 'Apr 2018', 'batch': '001_Apr2018_Day2', 'concentration': 7},
    {'date': 'Apr 2018', 'batch': '001_Apr2018_Day3', 'concentration': 2},
    {'date': 'Apr 2018', 'batch': '002_Apr2018_Day1', 'concentration': 6},
    {'date': 'Apr 2018', 'batch': '005_Apr2018_Day2', 'concentration': 3},
    {'date': 'May 2018', 'batch': '006_Apr2018_Day2', 'concentration': 6},
    {'date': 'May 2018', 'batch': '001_Apr2018_Day1', 'concentration': 7},
    {'date': 'May 2018', 'batch': '001_Apr2018_Day2', 'concentration': 9},
    {'date': 'May 2018', 'batch': '002_Apr2018_Day2', 'concentration': 12},
    {'date': 'May 2018', 'batch': '005_Apr2018_Day1', 'concentration': 7},
    {'date': 'May 2018', 'batch': '006_Apr2018_Day2', 'concentration': 3},
    {'date': 'May 2018', 'batch': '007_Apr2018_Day2', 'concentration': 6}
];

export default class API {

    /** Function for returning all users of the system (besides the super user */
    getUsers() {
        var token = sessionStorage.getItem("NangToken");
        {/*Send a get request to the users API*/}
        axios({
          method: 'get',
          url: 'https://deco3801-nang-it.uqcloud.net/API/users/',
          headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'JWT ${token}',
                'Accept' : 'application/json,application/xml',
          }
      }).then(function (response) {
            console.log("email set");
        }).catch(function (error) {
            alert("Sorry, there was an error fetching some data from our servers")
        });
    }

    /*Function for getting all drug data. Returns the concentrations data for all drugs over all time*/
    getDrugData() {
        {/*Send a get request to the drugs API*/}
        const token = sessionStorage.getItem("NangToken");
        axios({
          method: 'get',
          url: 'https://deco3801-nang-it.uqcloud.net/API/drugs/',
          headers: {
                'Authorization': 'JWT ${token}',
                'Accept' : 'application/json,application/xml',
          }
      }).then(function (response) {
            console.log("email set");
        }).catch(function (error) {
            alert("Sorry, there was an error fetching some data from our servers")
        });
    }

    /*Function for the data of a specific drug. Returns the concentrations data for a certain drugs over all time*/
    /** Data should be returned as follows
        {data:
        [{date: April 2018, sample: 001_April_2018_Day1, concentration: 130mL },
        {date: April 2018, sample: 001_April_2018_Day2, concentration: 170mL }....EG]
    }} */
    getSpecificDrugData(drug) {
        {/*Send a get request to the drugs API*/}
        const token = sessionStorage.getItem("NangToken");
        axios({
          method: 'get',
          url: 'https://deco3801-nang-it.uqcloud.net/API/drugs/',
          headers: {
                'Authorization': 'JWT ${token}',
                'Accept' : 'application/json,application/xml',
          }
      }).then(function (response) {
            console.log("drug data response recieved");
            return response
        }).catch(function (error) {
            //alert("Sorry, there was an error fetching some data from our servers")
            //Default to the backups
            if (drug == "Heroin") {
                //alert("return sample 1")
                return sampleData2
            } else {
                //alert("return sample 2")
                return sampleData
            }
        });
        //return sampleData
    }

    /*Function for appending a CSV to the drugs data (concentrations table in the database) table*/
    sendDrugData(csvFile) {
        {/*Send a POST request to the drug API*/}
        const token = sessionStorage.getItem("NangToken");
        axios({
          method: 'post',
          url: 'https://deco3801-nang-it.uqcloud.net/API/drugs/',
          headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'JWT ${token}',
                'Accept' : 'application/json,application/xml',
          },
          data: {
              drugCSV : csvFile
          }
      }).then(function (response) {
            console.log("email set");
        }).catch(function (error) {
            alert("Sorry, there was an error sending your file to our servers")
        });
    }

    /*Function for registering a new standard user*/
    registerNewAdmin(_username, _password) {
        alert("New account created")
        {/*Send a POST request to the drug API*/}
        const token = sessionStorage.getItem("NangToken");
        axios({
          method: 'post',
          url: 'https://deco3801-nang-it.uqcloud.net/API/users/',
          headers: {
                'Content-Type': 'application/json',
                'Authorization': 'JWT ${token}',
                'Accept' : 'application/json,application/xml',
          },
          data: {
            username: _username,
            password: _password
          }
      }).then(function (response) {
            console.log("email set");
        }).catch(function (error) {
            alert("Sorry, there was an error creating the new account")
            console.log(error)
        });
    }

    /** Function for getting the changelog data for the dashboard */
    getChangelogData() {
        return ([
          {
            "time": "3:50pm",
            "author": "Ben Tscharke",
            "message": "Added data 002_Cocaine"
          },
          {
            "time": "9:58am",
            "author": "Jake O'Brien",
            "message": "Manually updated data 004_Tobacco"
        }])
    }

    /** Function for getting the messageboard data for the dashboard */
    getMessageData() {
        return ([
          {
            "time": "3:52pm",
            "author": "Ben Tscharke",
            "message": "Appears there may be an issue in sample 001_Cocaine from site X"
          },
          {
            "time": "1:23pm",
            "author": "Jake O'Brien",
            "message": "I have checked the data from sample 002_MDMA from site X and it appears to be good"
          }])
    }
}
