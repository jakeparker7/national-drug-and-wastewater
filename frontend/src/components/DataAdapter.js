import API from './API'

// const sampleData = [
//     {'date': 'Apr 2018', 'batch': '001_Apr2018_Day1', 'concentration': 130},
//     {'date': 'Apr 2018', 'batch': '001_Apr2018_Day2', 'concentration': 160},
//     {'date': 'Apr 2018', 'batch': '001_Apr2018_Day3', 'concentration': 150},
//     {'date': 'Apr 2018', 'batch': '002_Apr2018_Day1', 'concentration': 130},
//     {'date': 'Apr 2018', 'batch': '005_Apr2018_Day2', 'concentration': 160},
//     {'date': 'May 2018', 'batch': '006_Apr2018_Day2', 'concentration': 150},
//     {'date': 'May 2018', 'batch': '001_Apr2018_Day1', 'concentration': 130},
//     {'date': 'May 2018', 'batch': '001_Apr2018_Day2', 'concentration': 160},
//     {'date': 'May 2018', 'batch': '002_Apr2018_Day2', 'concentration': 150},
//     {'date': 'May 2018', 'batch': '005_Apr2018_Day1', 'concentration': 130},
//     {'date': 'May 2018', 'batch': '006_Apr2018_Day2', 'concentration': 160},
//     {'date': 'May 2018', 'batch': '007_Apr2018_Day2', 'concentration': 150}
// ];
//
// const sampleData2 = [
//     {'date': 'Apr 2018', 'batch': '001_Apr2018_Day1', 'concentration': 5},
//     {'date': 'Apr 2018', 'batch': '001_Apr2018_Day2', 'concentration': 7},
//     {'date': 'Apr 2018', 'batch': '001_Apr2018_Day3', 'concentration': 2},
//     {'date': 'Apr 2018', 'batch': '002_Apr2018_Day1', 'concentration': 6},
//     {'date': 'Apr 2018', 'batch': '005_Apr2018_Day2', 'concentration': 3},
//     {'date': 'May 2018', 'batch': '006_Apr2018_Day2', 'concentration': 6},
//     {'date': 'May 2018', 'batch': '001_Apr2018_Day1', 'concentration': 7},
//     {'date': 'May 2018', 'batch': '001_Apr2018_Day2', 'concentration': 9},
//     {'date': 'May 2018', 'batch': '002_Apr2018_Day2', 'concentration': 12},
//     {'date': 'May 2018', 'batch': '005_Apr2018_Day1', 'concentration': 7},
//     {'date': 'May 2018', 'batch': '006_Apr2018_Day2', 'concentration': 3},
//     {'date': 'May 2018', 'batch': '007_Apr2018_Day2', 'concentration': 6}
// ];

const sampleData = [
    {'site_num': '1', 'day': '1', 'drug': 'MDMA', 'value': 130, 'date': 'Apr 2018'},
    {'site_num': '1', 'day': '2', 'drug': 'MDMA', 'value': 150, 'date': 'Apr 2018'},
    {'site_num': '1', 'day': '3', 'drug': 'MDMA', 'value': 70, 'date': 'Apr 2018'},
    {'site_num': '1', 'day': '4', 'drug': 'MDMA', 'value': 150, 'date': 'Apr 2018'},
    {'site_num': '2', 'day': '1', 'drug': 'MDMA', 'value': 130, 'date': 'Apr 2018'},
    {'site_num': '2', 'day': '2', 'drug': 'MDMA', 'value': 166, 'date': 'Apr 2018'},
    {'site_num': '2', 'day': '3', 'drug': 'MDMA', 'value': 70, 'date': 'Apr 2018'},
    {'site_num': '2', 'day': '4', 'drug': 'MDMA', 'value': 150, 'date': 'Apr 2018'},
    {'site_num': '1', 'day': '1', 'drug': 'MDMA', 'value': 120, 'date': 'May 2018'},
    {'site_num': '1', 'day': '2', 'drug': 'MDMA', 'value': 100, 'date': 'May 2018'},
    {'site_num': '1', 'day': '3', 'drug': 'MDMA', 'value': 98, 'date': 'May 2018'},
    {'site_num': '1', 'day': '4', 'drug': 'MDMA', 'value': 132, 'date': 'May 2018'},
    {'site_num': '1', 'day': '5', 'drug': 'MDMA', 'value': 150, 'date': 'May 2018'},
    {'site_num': '2', 'day': '1', 'drug': 'MDMA', 'value': 120, 'date': 'May 2018'},
    {'site_num': '2', 'day': '2', 'drug': 'MDMA', 'value': 100, 'date': 'May 2018'},
    {'site_num': '2', 'day': '3', 'drug': 'MDMA', 'value': 110, 'date': 'May 2018'},
    {'site_num': '2', 'day': '4', 'drug': 'MDMA', 'value': 150, 'date': 'May 2018'},
    {'site_num': '2', 'day': '5', 'drug': 'MDMA', 'value': 150, 'date': 'May 2018'}
];

const sampleData2 = [
    {'site_num': '1', 'day': '1', 'drug': 'MDMA', 'value': 80, 'date': 'Apr 2018'},
    {'site_num': '1', 'day': '2', 'drug': 'MDMA', 'value': 70, 'date': 'Apr 2018'},
    {'site_num': '1', 'day': '3', 'drug': 'MDMA', 'value': 68, 'date': 'Apr 2018'},
    {'site_num': '1', 'day': '4', 'drug': 'MDMA', 'value': 56, 'date': 'Apr 2018'},
    {'site_num': '2', 'day': '1', 'drug': 'MDMA', 'value': 80, 'date': 'Apr 2018'},
    {'site_num': '2', 'day': '2', 'drug': 'MDMA', 'value': 70, 'date': 'Apr 2018'},
    {'site_num': '2', 'day': '3', 'drug': 'MDMA', 'value': 68, 'date': 'Apr 2018'},
    {'site_num': '2', 'day': '4', 'drug': 'MDMA', 'value': 56, 'date': 'Apr 2018'},
    {'site_num': '1', 'day': '1', 'drug': 'MDMA', 'value': 76, 'date': 'May 2018'},
    {'site_num': '1', 'day': '2', 'drug': 'MDMA', 'value': 94, 'date': 'May 2018'},
    {'site_num': '1', 'day': '3', 'drug': 'MDMA', 'value': 67, 'date': 'May 2018'},
    {'site_num': '1', 'day': '4', 'drug': 'MDMA', 'value': 87, 'date': 'May 2018'},
    {'site_num': '1', 'day': '5', 'drug': 'MDMA', 'value': 99, 'date': 'May 2018'},
    {'site_num': '2', 'day': '1', 'drug': 'MDMA', 'value': 76, 'date': 'May 2018'},
    {'site_num': '2', 'day': '2', 'drug': 'MDMA', 'value': 94, 'date': 'May 2018'},
    {'site_num': '2', 'day': '3', 'drug': 'MDMA', 'value': 67, 'date': 'May 2018'},
    {'site_num': '2', 'day': '4', 'drug': 'MDMA', 'value': 87, 'date': 'May 2018'},
    {'site_num': '2', 'day': '5', 'drug': 'MDMA', 'value': 99, 'date': 'May 2018'}
];

export default class DataAdapter {

        stripSampleSite(sampleName) {
            var chop = require("underscore.string/chop");
            return chop(sampleName,3)
        }


        monthToIndex(month) {
            var decap = require("underscore.string/decapitalize");
            month = decap(month)
            if (month == "jan") {
                return 1;
            } else if (month == "feb") {
                return 2;
            } else if (month == "mar") {
                return 3;
            } else if (month == "apr") {
                return 4;
            } else if (month == "may") {
                return 5;
            } else if (month == "jun") {
                return 6;
            } else if (month == "jul") {
                return 7;
            } else if (month == "aug") {
                return 8;
            } else if (month == "sep") {
                return 9;
            } else if (month == "oct") {
                return 10;
            } else if (month == "nov") {
                return 11;
            } else if (month == "dec") {
                return 12;
            } else {
                return 0;
            }
        }

    /** Function for comparing dates. Returns true if date1 is after date 2 */
    isAfter(date1, date2) {
        var month1 = date1.slice(0,3)
        var year1 = date1.slice(4,8)
        var month2 = date2.slice(0,3)
        var year2 = date2.slice(4,8)
        var month1Int = this.monthToIndex(month1)
        var month2Int = this.monthToIndex(month2)
        if (parseInt(year1) > parseInt(year2)) {
            return true;
        } else if (month1Int > month2Int && parseInt(year1) >= parseInt(year2)) {
            return true;
        } else {
            //alert("Month 1 " + month1Int + " " + "Year 1 " + year1 + " Month 2" + month2Int + " " + " Year 2 " + year2)
            return false;
        }
    }

    /** Function for comparing dates. Returns true if date1 is begore date 2 */
    isBefore(date1, date2) {
        var month1 = date1.slice(0,3)
        var year1 = date1.slice(4,8)
        var month2 = date2.slice(0,3)
        var year2 = date2.slice(4,8)
        var month1Int = this.monthToIndex(month1)
        var month2Int = this.monthToIndex(month2)
        if (parseInt(year2) > parseInt(year1)) {
            return true;
        } else if (month2Int > month1Int && parseInt(year2) >= parseInt(year1)) {
            return true;
        } else {
            //alert("Month 1 " + month1Int + " " + "Year 1 " + year1 + " Month 2" + month2Int + " " + " Year 2 " + year2)
            return false;
        }
    }

    /** Function that processes a single drugs data (an array of JSON objects) appending
    returns the appropriate data structure to be displayed by the graphs */
    singleDrugData(data) {
        var testData = data
        var i;
        var months = [];
        var drug = ""
        var processedData = {
            title: "Monthly for ",
            keys: [],
            data: []
        };
        var startSite = '001'
        var dataKeys = []
        var drug = ""
        for (i = 0; i < testData.length; i++) {
            if (i == 0) {
                drug = testData[i].drug_id
            }
            if (!months.includes(testData[i].date)) {
                //Record that we have seen this month before
                months.push(testData[i].date)
                processedData['data'].push({'month' : testData[i].date})
            }
            var n = months.indexOf(testData[i].date)
            //alert(n)
            //Site number
            //var sampleSite = this.stripSampleSite(testData[i].batch)[0]
            var sampleSite = testData[i].site_no_id
            //intilize site value and color
            if (!dataKeys.includes(sampleSite)) {
                dataKeys.push(sampleSite)
            }
            //Check if this site exists yet
            if (processedData['data'][n].hasOwnProperty(sampleSite)) {
                //Append to the site value and dont change color
                processedData['data'][n][sampleSite] += testData[i].value
            } else {
                processedData['data'][n][sampleSite] = testData[i].value
                processedData['data'][n][sampleSite+"Color"] = "hsl(98, 70%, 50%)"
            }
        }
        processedData['keys'] = dataKeys
        processedData['title'] = "Monthly for " + drug
        console.log(processedData['data'])
        console.log(processedData['keys'])
        return {'processedData': processedData, 'rawData': testData}
    }

    /** Function that processes a single drugs data for data occuring after the date passeed in */
    singleDrugAfterDate(data, startDate) {
        var testData = data
        var i;
        var months = [];
        var processedData = {
            title: "Monthly for " + drug,
            keys: [],
            data: []
        };
        var startSite = '001'
        var dataKeys = []
        var drug = ""
        for (i = 0; i < testData.length; i++) {
            if (i == 0) {
                drug = testData[i].drug_id
            }
            var currentDate = testData[i].date
            if (!this.isAfter(currentDate, startDate)) {
                //alert(currentDate + "not after " + startDate)
                continue
            }
            if (!months.includes(testData[i].date)) {
                //Record that we have seen this month before
                months.push(testData[i].date)
                processedData['data'].push({'month' : testData[i].date})
            }
            var n = months.indexOf(testData[i].date)
            //alert(n)
            //Site number
            //var sampleSite = this.stripSampleSite(testData[i].batch)[0]
            var sampleSite = testData[i].site_no_id
            //intilize site value and color
            if (!dataKeys.includes(sampleSite)) {
                dataKeys.push(sampleSite)
            }
            //Check if this site exists yet
            if (processedData['data'][n].hasOwnProperty(sampleSite)) {
                //Append to the site value and dont change color
                processedData['data'][n][sampleSite] += testData[i].value
            } else {
                processedData['data'][n][sampleSite] = testData[i].value
                processedData['data'][n][sampleSite+"Color"] = "hsl(98, 70%, 50%)"
            }
        }
        processedData['keys'] = dataKeys
        processedData['title'] = "Monthly for " + drug
        console.log(processedData['data'])
        console.log(processedData['keys'])
        return {'processedData': processedData, 'rawData': testData}
    }

    /** Function that processes a single drugs data for data occuring after the date passeed in */
    singleDrugBeforeDate(data, startDate) {
        var testData = data
        var i;
        var months = [];
        var processedData = {
            title: "Monthly for " + drug,
            keys: [],
            data: []
        };
        var startSite = '001'
        var dataKeys = []
        var drug = ""
        for (i = 0; i < testData.length; i++) {
            if (i == 0) {
                drug = testData[i].drug_id
            }
            var currentDate = testData[i].date
            if (!this.isBefore(currentDate, startDate)) {
                //alert(currentDate + "not after " + startDate)
                continue
            }
            if (!months.includes(testData[i].date)) {
                //Record that we have seen this month before
                months.push(testData[i].date)
                processedData['data'].push({'month' : testData[i].date})
            }
            var n = months.indexOf(testData[i].date)
            //alert(n)
            //Site number
            //var sampleSite = this.stripSampleSite(testData[i].batch)[0]
            var sampleSite = testData[i].site_no_id
            //intilize site value and color
            if (!dataKeys.includes(sampleSite)) {
                dataKeys.push(sampleSite)
            }
            //Check if this site exists yet
            if (processedData['data'][n].hasOwnProperty(sampleSite)) {
                //Append to the site value and dont change color
                processedData['data'][n][sampleSite] += testData[i].value
            } else {
                processedData['data'][n][sampleSite] = testData[i].value
                processedData['data'][n][sampleSite+"Color"] = "hsl(98, 70%, 50%)"
            }
        }
        processedData['keys'] = dataKeys
        processedData['title'] = "Monthly for " + drug
        console.log(processedData['data'])
        console.log(processedData['keys'])
        return {'processedData': processedData, 'rawData': testData}
    }

    /** Function that processes a single drugs data for data occuring after the date passeed in */
    singleDrugBetweenDate(data, startDate, endDate) {
        var testData = data
        var i;
        var months = [];
        var processedData = {
            title: "Monthly for " + drug,
            keys: [],
            data: []
        };
        var startSite = '001'
        var dataKeys = []
        var drug = ""
        for (i = 0; i < testData.length; i++) {
            if (i == 0) {
                drug = testData[i].drug_id
            }
            var currentDate = testData[i].date
            if (!this.isBefore(currentDate, endDate) || !this.isAfter(currentDate,startDate)) {
                //alert(currentDate + "not after " + startDate)
                continue
            }
            if (!months.includes(testData[i].date)) {
                //Record that we have seen this month before
                months.push(testData[i].date)
                processedData['data'].push({'month' : testData[i].date})
            }
            var n = months.indexOf(testData[i].date)
            //alert(n)
            //Site number
            //var sampleSite = this.stripSampleSite(testData[i].batch)[0]
            var sampleSite = testData[i].site_no_id
            //intilize site value and color
            if (!dataKeys.includes(sampleSite)) {
                dataKeys.push(sampleSite)
            }
            //Check if this site exists yet
            if (processedData['data'][n].hasOwnProperty(sampleSite)) {
                //Append to the site value and dont change color
                processedData['data'][n][sampleSite] += testData[i].value
            } else {
                processedData['data'][n][sampleSite] = testData[i].value
                processedData['data'][n][sampleSite+"Color"] = "hsl(98, 70%, 50%)"
            }
        }
        processedData['keys'] = dataKeys
        processedData['title'] = "Monthly for " + drug
        console.log(processedData['data'])
        console.log(processedData['keys'])
        return {'processedData': processedData, 'rawData': testData}
    }

    /** Function that processes a single drugs data (an array of JSON objects) appending
    returns the appropriate data structure to be displayed by the graphs */
    multiDrugData(data) {
        var testData = data
        var i;
        var months = [];
        var processedData = {
            title: "Multidrug Data",
            keys: [],
            data: []
        };
        var dataKeys = []
        for (i = 0; i < testData.length; i++) {
            if (!months.includes(testData[i].date)) {
                //Record that we have seen this month before
                months.push(testData[i].date)
                processedData['data'].push({'month' : testData[i].date})
            }
            var n = months.indexOf(testData[i].date)
            //alert(n)
            //Site number
            //var sampleSite = this.stripSampleSite(testData[i].batch)[0]
            var sampleDrug = testData[i].drug_id
            //intilize site value and color
            if (!dataKeys.includes(sampleDrug)) {
                dataKeys.push(sampleDrug)
            }
            //Check if this site exists yet
            if (processedData['data'][n].hasOwnProperty(sampleDrug)) {
                //Append to the site value and dont change color
                processedData['data'][n][sampleDrug] += testData[i].value
            } else {
                processedData['data'][n][sampleDrug] = testData[i].value
                processedData['data'][n][sampleDrug+"Color"] = "hsl(98, 70%, 50%)"
            }
        }
        processedData['keys'] = dataKeys
        processedData['title'] = "Multidrug Data for " + dataKeys
        console.log(processedData['data'])
        console.log(processedData['keys'])
        return {'processedData': processedData, 'rawData': testData}
    }

    /** Function that processes a single drugs data for data occuring after the date passeed in */
    multiDrugAfterData(data, startDate) {
        var testData = data
        var i;
        var months = [];
        var processedData = {
            title: "Multidrug Data",
            keys: [],
            data: []
        };
        var dataKeys = []
        for (i = 0; i < testData.length; i++) {
            var currentDate = testData[i].date
            if (!this.isAfter(currentDate, startDate)) {
                //alert(currentDate + "not after " + startDate)
                continue
            }
            if (!months.includes(testData[i].date)) {
                //Record that we have seen this month before
                months.push(testData[i].date)
                processedData['data'].push({'month' : testData[i].date})
            }
            var n = months.indexOf(testData[i].date)
            //alert(n)
            //Site number
            //var sampleSite = this.stripSampleSite(testData[i].batch)[0]
            var sampleDrug = testData[i].drug_id
            //intilize site value and color
            if (!dataKeys.includes(sampleDrug)) {
                dataKeys.push(sampleDrug)
            }
            //Check if this site exists yet
            if (processedData['data'][n].hasOwnProperty(sampleDrug)) {
                //Append to the site value and dont change color
                processedData['data'][n][sampleDrug] += testData[i].value
            } else {
                processedData['data'][n][sampleDrug] = testData[i].value
                processedData['data'][n][sampleDrug+"Color"] = "hsl(98, 70%, 50%)"
            }
        }
        processedData['keys'] = dataKeys
        processedData['title'] = "Multidrug Data for " + dataKeys
        console.log(processedData['data'])
        console.log(processedData['keys'])
        return {'processedData': processedData, 'rawData': testData}
    }

    /** Function that processes a single drugs data for data occuring after the date passeed in */
    multiDrugBeforeData(data, startDate) {
        var testData = data
        var i;
        var months = [];
        var processedData = {
            title: "Multidrug Data",
            keys: [],
            data: []
        };
        var dataKeys = []
        for (i = 0; i < testData.length; i++) {
            var currentDate = testData[i].date
            if (!this.isBefore(currentDate, startDate)) {
                //alert(currentDate + "not after " + startDate)
                continue
            }
            if (!months.includes(testData[i].date)) {
                //Record that we have seen this month before
                months.push(testData[i].date)
                processedData['data'].push({'month' : testData[i].date})
            }
            var n = months.indexOf(testData[i].date)
            //alert(n)
            //Site number
            //var sampleSite = this.stripSampleSite(testData[i].batch)[0]
            var sampleDrug = testData[i].drug_id
            //intilize site value and color
            if (!dataKeys.includes(sampleDrug)) {
                dataKeys.push(sampleDrug)
            }
            //Check if this site exists yet
            if (processedData['data'][n].hasOwnProperty(sampleDrug)) {
                //Append to the site value and dont change color
                processedData['data'][n][sampleDrug] += testData[i].value
            } else {
                processedData['data'][n][sampleDrug] = testData[i].value
                processedData['data'][n][sampleDrug+"Color"] = "hsl(98, 70%, 50%)"
            }
        }
        processedData['keys'] = dataKeys
        processedData['title'] = "Multidrug Data for " + dataKeys
        console.log(processedData['data'])
        console.log(processedData['keys'])
        return {'processedData': processedData, 'rawData': testData}
    }

    /** Function that processes a single drugs data for data occuring after the date passeed in */
    multiDrugBetweenDate(data, startDate, endDate) {
        var testData = data
        var i;
        var months = [];
        var processedData = {
            title: "Multidrug Data",
            keys: [],
            data: []
        };
        var startSite = '001'
        var dataKeys = []
        for (i = 0; i < testData.length; i++) {
            var currentDate = testData[i].date
            if (!this.isBefore(currentDate, endDate) || !this.isAfter(currentDate,startDate)) {
                //alert(currentDate + "not after " + startDate)
                continue
            }
            if (!months.includes(testData[i].date)) {
                //Record that we have seen this month before
                months.push(testData[i].date)
                processedData['data'].push({'month' : testData[i].date})
            }
            var n = months.indexOf(testData[i].date)
            //alert(n)
            //Site number
            //var sampleSite = this.stripSampleSite(testData[i].batch)[0]
            var sampleDrug = testData[i].drug_id
            //intilize site value and color
            if (!dataKeys.includes(sampleDrug)) {
                dataKeys.push(sampleDrug)
            }
            //Check if this site exists yet
            if (processedData['data'][n].hasOwnProperty(sampleDrug)) {
                //Append to the site value and dont change color
                processedData['data'][n][sampleDrug] += testData[i].value
            } else {
                processedData['data'][n][sampleDrug] = testData[i].value
                processedData['data'][n][sampleDrug+"Color"] = "hsl(98, 70%, 50%)"
            }
        }
        processedData['keys'] = dataKeys
        processedData['title'] = "Multidrug Data for " + dataKeys
        console.log(processedData['data'])
        console.log(processedData['keys'])
        return {'processedData': processedData, 'rawData': testData}
    }


}
