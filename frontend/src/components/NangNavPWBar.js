import '../App.css';
import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';


const styles = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

const theme = createMuiTheme({
  palette: {
    primary: blue, // Purple and green play nicely together.
    secondary: { main: '#ffffff' }
  },
});

/** This component represents the navigation bar used throughout the public view of the website */
class NangNavBar extends Component {
  render() {
    return (
      <div>
      <MuiThemeProvider theme={theme}>
      <AppBar position="static" color='primary' style={{ marginBottom: 30 }}>
          <nav className="navbar navbar-expand-lg navbar-light">

            {/* Link to the main homepage*/}
            <Link
              className="nav-link"
              to="/">
              <Typography variant="title" color="secondary">
              NWDMP
              </Typography>
            </Link>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">

                {/* Link to the main home page */}
                <Link
                  className="nav-link"
                  to="/">
                  <Typography variant="subheading" color="secondary">
                  Home
                  </Typography>
                </Link>
              </ul>
            </div>
          </nav>
      </AppBar>
      </MuiThemeProvider>
    </div>
    )
  }
}

export default NangNavBar;
