import React, { Component } from 'react';
import MenuItem from '@material-ui/core/MenuItem';


class ViewData extends Component {
    render() {
        return (
            <div>
                <MenuItem value={""}>
                  <em>None</em>
                </MenuItem>
                    // 2016 //
                <MenuItem value={"Jan 2016"}>
                  <em>Jan 2016</em>
                </MenuItem>
                <MenuItem value={"Feb 2016"}>
                  <em>Feb 2016</em>
                </MenuItem>
                <MenuItem value={"Mar 2016"}>
                  <em>Mar 2016</em>
                </MenuItem>
                <MenuItem value={"Apr 2016"}>
                  <em>Apr 2016</em>
                </MenuItem>
                <MenuItem value={"May 2016"}>
                  <em>May 2016</em>
                </MenuItem>
                <MenuItem value={"Jun 2016"}>
                  <em>Jun 2016</em>
                </MenuItem>
                <MenuItem value={"Jul 2016"}>
                  <em>Jul 2016</em>
                </MenuItem>
                <MenuItem value={"Aug 2016"}>
                  <em>Aug 2016</em>
                </MenuItem>
                <MenuItem value={"Sep 2016"}>
                  <em>Sep 2016</em>
                </MenuItem>
                <MenuItem value={"Oct 2016"}>
                  <em>Oct 2016</em>
                </MenuItem>
                <MenuItem value={"Nov 2016"}>
                  <em>Nov 2016</em>
                </MenuItem>
                <MenuItem value={"Dec 2016"}>
                  <em>Dec 2016</em>
                </MenuItem>
                    // 2017 //
                <MenuItem value={"Jan 2017"}>
                  <em>Jan 2017</em>
                </MenuItem>
                <MenuItem value={"Feb 2017"}>
                  <em>Feb 2017</em>
                </MenuItem>
                <MenuItem value={"Mar 2017"}>
                  <em>Mar 2017</em>
                </MenuItem>
                <MenuItem value={"Apr 2017"}>
                  <em>Apr 2017</em>
                </MenuItem>
                <MenuItem value={"May 2017"}>
                  <em>May 2017</em>
                </MenuItem>
                <MenuItem value={"Jun 2017"}>
                  <em>Jun 2017</em>
                </MenuItem>
                <MenuItem value={"Jul 2017"}>
                  <em>Jul 2017</em>
                </MenuItem>
                <MenuItem value={"Aug 2017"}>
                  <em>Aug 2017</em>
                </MenuItem>
                <MenuItem value={"Sep 2017"}>
                  <em>Sep 2017</em>
                </MenuItem>
                <MenuItem value={"Oct 2017"}>
                  <em>Oct 2017</em>
                </MenuItem>
                <MenuItem value={"Nov 2017"}>
                  <em>Nov 2017</em>
                </MenuItem>
                <MenuItem value={"Dec 2017"}>
                  <em>Dec 2017</em>
                </MenuItem>
                    // 2018 //
                <MenuItem value={"Jan 2018"}>
                  <em>Jan 2018</em>
                </MenuItem>
                <MenuItem value={"Feb 2018"}>
                  <em>Feb 2018</em>
                </MenuItem>
                <MenuItem value={"Mar 2018"}>
                  <em>Mar 2018</em>
                </MenuItem>
                <MenuItem value={"Apr 2018"}>
                  <em>Apr 2018</em>
                </MenuItem>
                <MenuItem value={"May 2018"}>
                  <em>May 2018</em>
                </MenuItem>
                <MenuItem value={"Jun 2018"}>
                  <em>Jun 2018</em>
                </MenuItem>
                <MenuItem value={"Jul 2018"}>
                  <em>Jul 2018</em>
                </MenuItem>
                <MenuItem value={"Aug 2018"}>
                  <em>Aug 2018</em>
                </MenuItem>
                <MenuItem value={"Sep 2018"}>
                  <em>Sep 2018</em>
                </MenuItem>
                <MenuItem value={"Oct 2018"}>
                  <em>Oct 2018</em>
                </MenuItem>
                <MenuItem value={"Nov 2018"}>
                  <em>Nov 2018</em>
                </MenuItem>
                <MenuItem value={"Dec 2018"}>
                  <em>Dec 2018</em>
                </MenuItem>
                    // 2019 //
                <MenuItem value={"Jan 2019"}>
                  <em>Jan 2019</em>
                </MenuItem>
                <MenuItem value={"Feb 2019"}>
                  <em>Feb 2019</em>
                </MenuItem>
                <MenuItem value={"Mar 2019"}>
                  <em>Mar 2019</em>
                </MenuItem>
                <MenuItem value={"Apr 2019"}>
                  <em>Apr 2019</em>
                </MenuItem>
                <MenuItem value={"May 2019"}>
                  <em>May 2019</em>
                </MenuItem>
                <MenuItem value={"Jun 2019"}>
                  <em>Jun 2019</em>
                </MenuItem>
                <MenuItem value={"Jul 2019"}>
                  <em>Jul 2019</em>
                </MenuItem>
                <MenuItem value={"Aug 2019"}>
                  <em>Aug 2019</em>
                </MenuItem>
                <MenuItem value={"Sep 2019"}>
                  <em>Sep 2019</em>
                </MenuItem>
                <MenuItem value={"Oct 2019"}>
                  <em>Oct 2019</em>
                </MenuItem>
                <MenuItem value={"Nov 2019"}>
                  <em>Nov 2019</em>
                </MenuItem>
                <MenuItem value={"Dec 2018"}>
                  <em>Dec 2019</em>
                </MenuItem>
                    // 2020 //
                <MenuItem value={"Jan 2020"}>
                  <em>Jan 2020</em>
                </MenuItem>
                <MenuItem value={"Feb 2020"}>
                  <em>Feb 2020</em>
                </MenuItem>
                <MenuItem value={"Mar 2020"}>
                  <em>Mar 2020</em>
                </MenuItem>
                <MenuItem value={"Apr 2020"}>
                  <em>Apr 2020</em>
                </MenuItem>
                <MenuItem value={"May 2020"}>
                  <em>May 2020</em>
                </MenuItem>
                <MenuItem value={"Jun 2020"}>
                  <em>Jun 2020</em>
                </MenuItem>
                <MenuItem value={"Jul 2020"}>
                  <em>Jul 2020</em>
                </MenuItem>
                <MenuItem value={"Aug 2020"}>
                  <em>Aug 2020</em>
                </MenuItem>
                <MenuItem value={"Sep 2020"}>
                  <em>Sep 2020</em>
                </MenuItem>
                <MenuItem value={"Oct 2020"}>
                  <em>Oct 2020</em>
                </MenuItem>
                <MenuItem value={"Nov 2020"}>
                  <em>Nov 2020</em>
                </MenuItem>
                <MenuItem value={"Dec 2020"}>
                  <em>Dec 2020</em>
                </MenuItem>
            </div>
        )
    }
}

export default ViewData;
