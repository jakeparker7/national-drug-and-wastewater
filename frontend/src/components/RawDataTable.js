

import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

class RawDataTable extends Component {

  render() {
    {/* data passed in is an array of rows with an id, author and message */}
    const rows = this.props.data
    return(
      <div>
      <h1> {this.props.title} </h1>
      <Table>

      {/* Table header */}
      <TableHead>
          <TableRow>
            <TableCell>Location</TableCell>
            <TableCell>Date</TableCell>
            <TableCell>Drug</TableCell>
            <TableCell>Concentration (standard doses / day / 1000)</TableCell>
          </TableRow>
        </TableHead>

      {/* Map the data into the table */}
      {rows.map(row => {
            return (
              <TableRow key={row.id}>
                <TableCell>{row.site_no_id}</TableCell>
                <TableCell>{row.date}</TableCell>
                <TableCell>{row.drug_id}</TableCell>
                <TableCell>{row.value}</TableCell>
              </TableRow>
            );
          })}
      </Table>
      </div>
    )
  }
}

export default RawDataTable;
