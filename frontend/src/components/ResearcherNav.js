import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import Drawer from '@material-ui/core/Drawer';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from 'react-router-dom';
import ResearcherDrawer from './ResearcherDrawer'

const theme = createMuiTheme({
  palette: {
    primary: blue, // Purple and green play nicely together.
    secondary: { main: '#ffffff' }
  },
});

const style = {
	drawer: {
		top: '64px',
		width: '50%'
	}
}



class ResearcherNav extends Component {

  state = {
      left: false
    };

    toggleDrawer = (side, open) => () => {
      this.setState({
        [side]: open,
      });
    };

  render() {

    return (
      <MuiThemeProvider theme={theme}>
      <div>
        <AppBar position="static" color="primary">
          <Toolbar>
            <IconButton color="inherit" aria-label="Menu" onClick={this.toggleDrawer('left', true)}>
              <MenuIcon />
            </IconButton>
            <Drawer open={this.state.left} onClose={this.toggleDrawer('left', false)}
              containerStyle={style.drawer}>
              <ResearcherDrawer/>
            </Drawer>
            <Typography variant="title" color="inherit">
              Researcher Page
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
      </MuiThemeProvider>
    );
  }
}


export default ResearcherNav;
