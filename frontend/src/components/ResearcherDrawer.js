import React, { Component } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle'
import Drawer from '@material-ui/core/Drawer';

class ResearcherDrawer extends Component {

  render() {
    return (
      <div>
        <MenuItem onClick={this.handleClose}>
          <Link
            className="nav-link"
            to="/researcher-portal/dashboard">
            Dashboard{" "}
          </Link>
        </MenuItem>
        <MenuItem onClick={this.handleClose}>
          <Link
            className="nav-link"
            to="/researcher-portal/edit-data">
            Edit Data{" "}
          </Link>
        </MenuItem>
        <MenuItem onClick={this.handleClose}>
          <Link
            className="nav-link"
            to="/researcher-portal/view-data">
            View Data{" "}
          </Link>
        </MenuItem>
        <MenuItem onClick={this.handleClose}>
          <Link
            className="nav-link"
            to="/researcher-portal/admin">
            Admin{" "}
          </Link>
        </MenuItem>
      </div>
    )
  }
}

export default ResearcherDrawer;
