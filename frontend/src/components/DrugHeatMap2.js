import '../App.css';
import React, { Component } from 'react';
import { render } from 'react-dom'
import SVG from 'react-inlinesvg';
import AustraliaSVG from '../resources/AustraliaHigh.svg';

class DrugHeatMap2 extends Component {

  render() {
    return (
      <div>
        <h2> {this.props.title} </h2>
        <div style={{ height: 650 }}>
        <AustraliaSVG/>
        </div>
      </div>
    )
  }
}

export default DrugHeatMap2;
