import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

/** This form is used for researchers to add a new researcher to the database */
class AddResearcherForm extends Component {
  render() {
    return(
    <Paper className="paper login" elevation={1} style={{width:"50vh", height:"50vh"}}>
      <Typography variant="headline" align="center" gutterBottom="true" offset="2" text-align="center">
        Login
      </Typography>
      <br></br>

      {/* Username entry field */}
      <TextField
          autoFocus
          margin="dense"
          id="user"
          label="Username"
          type="text"
        />
      <br></br>

      {/* Password entry field */}
      <TextField
            autoFocus
            margin="normal"
            id="password"
            label="Password"
            type="password"
          />
          <br></br>

        {/* Submission button */}
        <Button>
          <Link
            className="nav-link"
            to="/researcher-portal">
            Login{" "}
          </Link>
        </Button>

      </Paper>
    )
  }
}

export default LoginForm
