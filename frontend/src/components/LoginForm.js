import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router'
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import AccountCircle from '@material-ui/icons/AccountCircle'
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from '@material-ui/core/styles';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import axios from 'axios';
import blue from '@material-ui/core/colors/blue';
import ResearcherDashboard from "../pages/researcher_pages/researcher-dashboard"
import { Route } from 'react-router-dom';
import { withRouter } from "react-router-dom";

const theme = createMuiTheme({
  palette: {
    primary: blue, // Purple and green play nicely together.
    secondary: { main: '#ffffff' }
  },
});

class LoginForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username:'',
      password:'',
      loginStatus: false,
      token:""
    }
  }

  handlePassChange = this.handlePassChange.bind(this);
  handleUserChange = this.handleUserChange.bind(this);

  handleUserChange(event) {
    this.setState({
      username: event.target.value,
    });
  };

  handlePassChange(event) {
    this.setState({
      password: event.target.value,
    });
    };

    confirmLogin(token) {
        this.props.history.push('/researcher-portal');
    }

  handleClick = (event) => {
    {/* Setup the base URL for the auth api*/}
    var apiBaseUrl = "https://deco3801-nang-it.uqcloud.net/API/";
    var self = this;
    var payload={
     "username":this.state.username,
     "password":this.state.password
    }

    {/*Post the request*/}
    axios({
          method: 'post',
          url: 'https://deco3801-nang-it.uqcloud.net/API/api-token-auth/',
          headers: {
                'Content-Type': 'application/json'
          },
          data: {
            username: this.state.username,
            password: this.state.password
          }
        }).then(function (response) {
            console.log("Response");
            console.log(response.data);
            console.log(response.data.token);
            {/*OK Response*/}
            if(response.data.token != "") {
                console.log("Login successfull");
                self.setState({
                  token: response.data.token
                });
                self.setState({loginStatus:true});
                {/*Set the window session token*/}
                sessionStorage.setItem('NangToken', response.data.token);
                self.confirmLogin(response.data.token);
            } else {
              {/*Non valid / bad responses*/}
                self.setState({loginStatus:false});
                console.log("Username password do not match");
                alert("username password do not match");
            }
    }).catch(function (error) {
        self.setState({loginStatus:false});
        alert("There was an error!");
        console.log(error);
        {/*Set the window session token*/}
        sessionStorage.setItem('NangToken', "Token");
        self.confirmLogin("Test");


    });

  };

  render() {
    sessionStorage.removeItem('NangToken');
    return(
    <div>
    <MuiThemeProvider theme={theme}>
    <Paper className="paper login" elevation={5} style={{width:"30vh", height:"30vh"}}>
    <div style={{display: 'flex', justifyContent: 'center'}}>
        <AccountCircle />
      </div>
        <br></br>
      <div style={{display: 'flex', justifyContent: 'center'}}>
          <TextField
              autoFocus
              margin="dense"
              id="user"
              label="Username"
              type="text"
              value={this.state.username}
              onChange={this.handleUserChange}
            />
        </div>
        <br></br>
        <div style={{display: 'flex', justifyContent: 'center'}}>
        <TextField
              autoFocus
              margin="normal"
              id="password"
              label="Password"
              type="password"
              value={this.state.password}
              onChange={this.handlePassChange}
            />
            </div>
            <br></br>
            <div style={{display: 'flex', justifyContent: 'center'}}>
            <Button variant="contained" color="primary" onClick={(event) => this.handleClick(event)}>
            Login
            </Button>
            </div>
          </Paper>
          </MuiThemeProvider>
          </div>
    )
  }
}

export default withRouter(LoginForm);
