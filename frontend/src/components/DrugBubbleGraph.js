import '../App.css';
import React, { Component } from 'react';
import { ResponsiveBubble } from '@nivo/circle-packing'

class DrugBubbleGraph extends Component {

  render() {
    return (
      <div>
        <h2> {this.props.title} </h2>
        <div style={{ height: 400 }}>
          <ResponsiveBubble
            root={this.props.data}
            margin={{
                "top": 20,
                "right": 20,
                "bottom": 20,
                "left": 20
            }}
            identity="name"
            value="loc"
            colors="nivo"
            colorBy="name"
            padding={6}
            labelTextColor="inherit:darker(0.8)"
            borderWidth={2}
            defs={[
                {
                    "id": "lines",
                    "type": "patternLines",
                    "background": "none",
                    "color": "inherit",
                    "rotation": -45,
                    "lineWidth": 5,
                    "spacing": 8
                }
            ]}
            fill={[
                {
                    "match": {
                        "depth": 1
                    },
                    "id": "lines"
                }
            ]}
            animate={true}
            motionStiffness={90}
            motionDamping={12}
            />
        </div>
      </div>
    )
  }
}

export default DrugBubbleGraph;
