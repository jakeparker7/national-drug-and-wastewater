import '../App.css';
import React, { Component } from 'react';
import { ResponsiveHeatMap } from '@nivo/heatmap'

class DrugHeatMap extends Component {

  render() {
    return (
      <div>
        <h2> {this.props.title} </h2>
        <div style={{ height: 400 }}>
        <ResponsiveHeatMap
          data={this.props.data}
          colors="reds"
          keys={[
              "0",
              "1",
              "2",
              "3",
              "4",
              "5",
              "6",
              "7",
              "8",
              "9",
              "10"
          ]}
          indexBy="y"
          margin={{
              "top": 100,
              "right": 60,
              "bottom": 60,
              "left": 60
          }}
          forceSquare={true}
          axisTop={{
              "orient": "top",
              "tickSize": 5,
              "tickPadding": 5,
              "tickRotation": -90,
              "legend": "",
              "legendOffset": 36
          }}
          axisLeft={{
              "orient": "left",
              "tickSize": 5,
              "tickPadding": 5,
              "tickRotation": 0,
              "legend": "country",
              "legendPosition": "center",
              "legendOffset": -40
          }}
          cellOpacity={1}
          cellBorderColor="inherit:darker(0.4)"
          labelTextColor="inherit:darker(1.8)"
          defs={[
              {
                  "id": "lines",
                  "type": "patternLines",
                  "background": "inherit",
                  "color": "rgba(0, 0, 0, 0.1)",
                  "rotation": -45,
                  "lineWidth": 4,
                  "spacing": 7
              }
          ]}
          fill={[
              {
                  "id": "lines"
              }
          ]}
          animate={true}
          motionStiffness={80}
          motionDamping={9}
          hoverTarget="cell"
          cellHoverOthersOpacity={0.25}
      />
      </div>
      </div>
    )
  }
}

export default DrugHeatMap;
