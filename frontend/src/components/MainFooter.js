import '../App.css';
import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';


const styles = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

const theme = createMuiTheme({
  palette: {
    primary: blue, // Purple and green play nicely together.
    secondary: { main: '#ffffff' }
  },
});

class MainFooter extends Component {
  render() {
    return (
      <div>
      <MuiThemeProvider theme={theme}>
      <div>
      <AppBar position="static" color='primary' style={{ marginTop: 20 }}>
          <nav className="navbar navbar-expand-lg navbar-light">
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">
                <Link
                  className="nav-link"
                  to="/">
                  <Typography variant="body1" color="secondary">
                  Home
                  </Typography>
                </Link>
                <Link
                  className="nav-link"
                  to="/reports"
                  color="secondary">
                  <Typography variant="body1" color="secondary">
                  Reports
                  </Typography>
                </Link>
                <Link
                  className="nav-link"
                  to="/visualisations">
                  <Typography variant="body1" color="secondary">
                  Visualisations
                  </Typography>
                </Link>
                <Link
                  className="nav-link"
                  to="/about">
                  <Typography variant="body1" color="secondary">
                  About us
                  </Typography>
                </Link>
              </ul>
              <div style={{display: 'flex',  justifyContent:'left', alignItems:'left', margin: "20"}}>
                <Typography variant="body1" color="secondary">
                Contact us: XXXXX
                Email: XXXXXX
                </Typography>
                <Typography variant="body1" color="secondary">
                Copyright 2018
                </Typography>
                </div>
            </div>
          </nav>
      </AppBar>
      </div>
      </MuiThemeProvider>
    </div>

    )
  }
}

export default MainFooter;
